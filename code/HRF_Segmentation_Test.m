 clc, clear, close all
%% Load utilies
addpath(genpath('.\\code\'));
addpath(genpath('.\\images\'));
%% Read enhanced images
number = '1';
pathname_e = '.\results\images_enhanced\HRF\healthy\';
results=zeros(15,6,10);%i=15->number of images,10->methods number
for i = 1:15
%% Load Image
disp(i)
number = num2str(i);
im = imread([number '_h.jpg']);
im = im(:,:,2);
%% Load Mask
immask = imread([number '_h_mask.tif']);
immask = im2bw(immask);
%% Mask Erode
immask = imerode(immask, strel('disk', 10));
%% Load GT
imgt = logical(imread([number,'_h.tif'])); %ground truht
imgt = imgt .* double(immask);
%% Load Enhanced Images
load([pathname_e 'bowler-' number '.mat']);
load([pathname_e 'clahe-' number '.mat']);
load([pathname_e 'line-' number '.mat']);
load([pathname_e 'neurite-' number '.mat']);
load([pathname_e 'pctv-' number '.mat']);
load([pathname_e 'pctn-' number '.mat']);
load([pathname_e 'top-hat-' number '.mat']);
load([pathname_e 'vesselness-' number '.mat']);
load([pathname_e 'volume-ratio-' number '.mat']);
load([pathname_e 'wavelet-' number '.mat']);
%% sorting the mess!!!!!!!!!!
images = {imda};%,Vmax imf,imf2,imP,RVR,he,S_sum,imw,imbcosfire};
names = {'bowler-hat'};%,'vess','neu','pctn','pctv','rvr','clahe','top-hat','wavelet','line'};

%% Segmentation
% n = 160; c = -7;
n = 160; c = -12;

    for j=1:length(images)
        %% sorting the mess!!!!!!!!!!
        ime = images{j};
        %ime = ime .* double(immask);
        name = names{j};
        disp(name);
        %% Local Threshold
        [imth,~] = MeanThreshold2D(ime,n,c);
        %% Masking
    %     imth = imth .* double(immask);
        %% Cleaning Up
        imlabel = bwlabel(imth);
        s = regionprops(imlabel,ime, 'Area', 'MeanIntensity');
        area = [s.Area];
        mi = [s.MeanIntensity];
        idx = find((area>400));
    %     idx = find((area>300 & mi>0.1));
        imth = ismember(imlabel, idx);
        %% connectivity
        CC = bwconncomp(imth);
        objects=CC.NumObjects;
        %% Stats   
        [TP,TN,FP,FN] = recal(imgt,imth);
        [Sensitivity,Specificity,Precision,FPR,FNR,ACC] = stats(TP,TN,FN,FP);
        disp([Sensitivity,Specificity,ACC]);%Precision,FPR,FNR,ACC,objects]);
        %% RGB
        imrgb = NetworkRGB3D(imgt,logical(FP),logical(FN),logical(TP));
        %% Save
    %     imwrite(imrgb,[pathname_rgb 'rgb-' number '-' name '.png']);
    results(i,:,j) =[Sensitivity,Specificity,Precision,FPR,FNR,ACC];
    end
end
%% Mean and Std 
fid=fopen('Results.txt','w');
methods ={'Bowler','Vessel','Neurit','PctNeu','PctVess','Volume','CLAHE','TopHat','Wavelet','Line'};
fprintf(fid,'\t\t\tSE_mean\t\t SE_std\t\t SP_mean\t SP_std\t\t ACC_mean\t ACC_std\n');

for k=1:10
    SeMean = round(mean(results(:,1,k)),3);      SeStd = round(std(results(:,1,k)),3); 
    SpMean = round(mean(results(:,2,k)),3);      SpStd = round(std(results(:,2,k)),3); 
    AccMean = round(mean(results(:,6,k)),3);     AccStd = round(std(results(:,6,k)),3);
    name =cell2mat( methods(k));
    fprintf(fid,'%s\t\t%0.3f\t\t %0.3f\t\t %0.3f\t\t %0.3f\t\t %0.3f\t\t %0.3f \n', name, SeMean, SeStd, SpMean, SpStd,AccMean,AccStd);
    
end
fclose(fid);
%%
