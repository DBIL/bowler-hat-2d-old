function se = BOEllipse2D(s,a)
%% Ellipse SE
s = round(s); 
sm = max(s);
[xg,yg] = meshgrid(-sm:sm,-sm:sm);
[xg,yg] = BORotateVector2D(xg,yg,0,0,a);
se = ( (xg/s(1)).^2 + (yg/s(2)).^2 ) <= 1;
end

function [xp,yp] = BORotateVector2D(x,y,x0,y0,angle)
xp =  (x-x0)*cos(angle)-(y-y0)*sin(angle); % Translate and rotate coords.
yp =  (x-x0)*sin(angle)+(y-y0)*cos(angle);
%xp =  (x-x0)*cos(angle)+(y-y0)*sin(angle); % Translate and rotate coords.
%yp = -(x-x0)*sin(angle)+(y-y0)*cos(angle);
end