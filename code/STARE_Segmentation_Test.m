 clc, clear, close all
%% Load utilies
addpath(genpath('.\\code\'));
addpath(genpath('.\\images\'));
%% Read enhanced images
number = '1';
pathname_e = '.\results\images_enhanced\STARE\';
numbers = {'0001','0002','0003','0004','0005','0044','0077','0081','0082','0139','0162','0163','0235','0236','0239','0240','0255','0291','0319','0324'};
results=zeros(20,6,10);

for i = 1:length(numbers)
%% Load Image
disp(i)
number = numbers{i};
im = imread(['im' number '.ppm']);
im = im(:,:,2);
%% Load Mask
immask = imread(['maskim' number '.ppm']);
immask = im2bw(immask);
%% Mask Erode
immask = imerode(immask, strel('disk', 10));
%% Load GT
imgt = logical(imread(['im' number '.ah.ppm'])); %ground truht
imgt = imgt .* double(immask);
%% Load Enhanced Images
load([pathname_e 'bowler-' number '.mat']);
load([pathname_e 'clahe-' number '.mat']);
load([pathname_e 'line-' number '.mat']);
load([pathname_e 'neurite-' number '.mat']);
load([pathname_e 'pctv-' number '.mat']);
load([pathname_e 'pctn-' number '.mat']);
load([pathname_e 'top-hat-' number '.mat']);
load([pathname_e 'vesselness-' number '.mat']);
load([pathname_e 'volume-ratio-' number '.mat']);
load([pathname_e 'wavelet-' number '.mat']);
%% sorting the mess!!!!!!!!!!
images = {imda};%,Vmax imf,imf2,imP,RVR,he,S_sum,imw,imbcosfire};
names = {'bowler-hat'};%,'vess','neu','pctn','pctv','rvr','clahe','top-hat','wavelet','line'};

%% Segmentation
% n = 160; c = -7;
n = 120; c = -16;
    for j=1:length(images)
        %% sorting the mess!!!!!!!!!!
        ime = images{j};
        ime = ime .* double(immask);
        name = names{j};
        disp(name);
        %% Local Threshold
        [imth,~] = MeanThreshold2D(ime,n,c);
        %% Masking
    %     imth = imth .* double(immask);
        %% Cleaning Up
        imlabel = bwlabel(imth);
        s = regionprops(imlabel,ime, 'Area', 'MeanIntensity');
        area = [s.Area];
        mi = [s.MeanIntensity];
        idx = find((area>400));
%          idx = find((area>400 & mi>0.1));
        imth = ismember(imlabel, idx);
        %% connectivity
        CC = bwconncomp(imth);
        objects=CC.NumObjects;
        
        CC2 = bwconncomp(imgt);
        objects2=CC2.NumObjects;
        %% Stats   
        [TP,TN,FP,FN] = recal(imgt,imth);
        [Sensitivity,Specificity,Precision,FPR,FNR,ACC] = stats(TP,TN,FN,FP);
        disp([Sensitivity,Specificity,ACC]);
    %     T(j,:) = {name,Sensitivity,Specificity,Precision,FPR,FNR,ACC};
        %% RGB
        imrgb = NetworkRGB3D(imgt,logical(FP),logical(FN),logical(TP));
        results(i,:,j) =[Sensitivity,Specificity,Precision,FPR,FNR,ACC];
    end
end
%% Mean and Std 
fid=fopen('ResultsStare.txt','w');
methods ={'Bowler','Vessel','Neurit','PctNeu','PctVess','Volume','CLAHE','TopHat','Wavelet','Line'};
fprintf(fid,'\t\t\tSE_mean\t\t SE_std\t\t SP_mean\t SP_std\t\t ACC_mean\t ACC_std\n');

for k=1:10
    SeMean = round(mean(results(:,1,k)),3);      SeStd = round(std(results(:,1,k)),3); 
    SpMean = round(mean(results(:,2,k)),3);      SpStd = round(std(results(:,2,k)),3); 
    AccMean = round(mean(results(:,6,k)),3);     AccStd = round(std(results(:,6,k)),3);
    name =cell2mat( methods(k));
    fprintf(fid,'%s\t\t%0.3f\t\t %0.3f\t\t %0.3f\t\t %0.3f\t\t %0.3f\t\t %0.3f \n', name, SeMean, SeStd, SpMean, SpStd,AccMean,AccStd);
    
end
fclose(fid);

