%% Clear
clc; close all; clear;
%% Ellipse
s = [20,5];
o = 0:pi/12:pi;
%%
figure;
for i=1:length(o)
    se = BOEllipse2D(s,o(i));
    figure; imagesc(se); colormap gray; axis off; axis equal; axis tight;
    waitforbuttonpress
end