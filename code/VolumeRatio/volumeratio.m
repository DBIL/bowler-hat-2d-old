function Vmax = volumeratio(im,sigma,gamma,t)
%%  Hessian-Based Vessel Enhancement Based on Volume Ratio
%   
%   REFERENCES:
%       Tim Jerman, et al., 
%       Blob Enhancement and Visualization forImproved Intracranial
%       Aneurysm Detection, IEEE Transactions on Visualization and 
%       Computer Graphics, 22, 6, 1705-1717, 2016
%
%   AUTHORS:
%       Chas Nelson, Boguslaw Obara, Cigdem Sazak (Durham University)

%% Parameters
if nargin<4 || isempty(t);  t = 0.9;  end %tao for regularised volume ratio
if nargin<3 || isempty(gamma);  gamma = 2;  end %gamma factor for normalising Hessian derivatives
if nargin<2 || isempty(sigma);  sigma = 1:3;  end %scale options, generally s_min:s_max (i.e. minimum expected size to maximum expected size)

%% Start Loop
[m,n] = size(im);
Vp = zeros(m,n,length(sigma));%Jerman et al regularised volume ratio

for i=1:length(sigma)
    %% Second Derivatives - Hessian Matrix
    [Hxx,Hxy,Hyy] = HessianMatrix2D(im,sigma(i));
    
    %% Normalized Derivative - Scale
    Hxx = power(sigma(i),gamma)*Hxx;
    Hxy = power(sigma(i),gamma)*Hxy;
    Hyy = power(sigma(i),gamma)*Hyy;
    
    %% Eigen Matrix - values and vectors
    %[L1,L2] = BOEigenMatrix2x2(Hxx,Hxy,Hxy,Hyy);An analytic solution for the eigenvalues of 2x2 matrices.
    [L1,L2] = EigenMatrix2x2M(Hxx,Hxy,Hxy,Hyy);
    [~,L2] = EigenSort2x2M(L1,L2);
    L2 = -L2;
    
    %% Regularised Volume Ratio (Jerman et al, 2016)
    L3 = L2;
    Lrho = zeros(size(L3));
    Lrho(L3 >= (t * max(L3(:)))) = L3(L3 >= (t * max(L3(:))));
    Lrho((L3 < (t * max(L3(:)))) & (L3 > 0)) = t * max(L3(:));
    
    V = (L2.^2) .* (Lrho - L2) .* ((3.*ones(size(L3)))./(L2 + Lrho)).^3;
    V(L2 > (Lrho/2) & (Lrho/2) > 0) = 1;
    V(L2 <= 0 | Lrho <= 0) = 0;
    
    Vp(:,:,i) = V;
    
end
%% Calculate Maximum Image Over All Scales
[Vmax,~] = max(Vp,[],3);

end