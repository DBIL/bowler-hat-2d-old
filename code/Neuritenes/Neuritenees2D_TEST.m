clear all; clc; close all;
%% Read Image
file='ER1.png';
im=imread(file);
[pathstr,name,ext] = fileparts(file);
name = strcat(name,'Neurite');
im = rgb2gray(im);
%% Normalise the image
im = double(im); im = (im - min(im(:))) / (max(im(:)) - min(im(:)));
%  im = imcomplement(im);
%% Filter the image
sigma = 3
[imf,Vx,Vy] = NeuriteneesFilter2D(im,sigma);
%% result
% figure,imshow(imf);
% figure,imshow(Vx);
% figure,imshow(Vy);
%%
figure; imagesc(imf); colormap gray; axis equal; axis tight; axis off;
name = strcat(name, '.png');
saveFunction(imf,'2D',name);