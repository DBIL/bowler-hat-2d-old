close all
im = imread('01_test.tif');
im = rgb2gray(im);
imm = imread('01_manual1.gif');
immask = imread('01_test_mask.gif');
imf = imread('enhanced.png');
imf = imread('01_testVessel.png');


[B,L] = bwboundaries(immask>0,'noholes');
figure; 
imagesc(im); colormap gray; axis off; axis equal; axis tight;
hold on
for k = 1:length(B)
   boundary = B{k};
   plot(boundary(:,2), boundary(:,1), 'r', 'LineWidth', 2)
end

figure; 
imagesc(imm); colormap gray; axis off; axis equal; axis tight;
hold on
for k = 1:length(B)
   boundary = B{k};
   plot(boundary(:,2), boundary(:,1), 'r', 'LineWidth', 2)
end

se = strel('disk', 15);
immask2 = imerode(immask>0,se);
[B,L] = bwboundaries(immask2>0,'noholes');

figure; 
imagesc(imf); colormap gray; axis off; axis equal; axis tight;
hold on
for k = 1:length(B)
   boundary = B{k};
   plot(boundary(:,2), boundary(:,1), 'r', 'LineWidth', 2)
end