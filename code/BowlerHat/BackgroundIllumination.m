clc; clear; close all;

%% Image Create
[imOrig,gt]=Synthetic('curve','no'); % curve , blob, line,  cross, plane

%% Paramaters
mag = 1;%6;
dir = 30;%83;

fprintf('Image with background illumination of magnitude %d along %d degrees;\n',mag,dir);
    
%% Add artificial background illumination
[m,n] = size(imOrig);
[X,Y] = ndgrid(1:m,1:n);
bgill = ((X-m/2).*cosd(dir) + (Y-n/2).*sind(dir));
bgill = mat2gray(bgill);

im = mat2gray(imOrig + mag*bgill);
% figure, subplot(121), imshow(imOrig), subplot(122), imshow(im)
% imwrite(im,sprintf('bgill_%d_%d.png',mag,dir))

%% Proposed Method
si = 40; no=20;
imda = Granulo2D(im,si,no);
imda= Normalize(imda);
imwrite(imda,sprintf('imda_%d_%d.png',mag,dir))

%% Vesselness
im1 = imcomplement(im);
sigma = 0.2:0.5:6; gamma = 2; beta = 0.5; c = 15; wb = true;
[~,Vmax] = Vesselness2D(im1,sigma,gamma,beta,c,wb);
Vmax = Normalize(Vmax);
imwrite(Vmax,sprintf('Vmax_%d_%d.png',mag,dir))

%% Neutriness
sigma = 6;
[imf,~,~] = NeuriteneesFilter2D(im,sigma);
imf= Normalize(imf);
imwrite(imf,sprintf('imf_%d_%d.png',mag,dir))

%% Volume Ratio
sigma = 1:3;
gamma = 2;
t = 0.9;
RVR = volumeratio(im,sigma,gamma,t);
RVR = Normalize(RVR);
imwrite(RVR,sprintf('RVR_%d_%d.png',mag,dir));

%% PCT Vesselness 
nscale              = 10; 
norient             = 4;
minWaveLength       = 2.1;
mult                = 2; 
sigmaOnf            = 0.65;
k                   = 8;
cutOff              = 0.15;
g                   = 5;               
noiseMethod         = -2;
beta = 20;c = 15;%Vesselness parameters
imP = PhaseVesselnessFilter2D(beta,c,im1,nscale,norient,minWaveLength,mult,sigmaOnf,k,cutOff, g, noiseMethod);
imP = Normalize(imP);
imwrite(imP,sprintf('imP_%d_%d.png',mag,dir))
clear nscale norient  minWaveLength mult sigmaOnf k noiseMethod;

%% PCT NEuriteness 
nscale              = 10; 
norient             = 4;
minWaveLength       = 3;
mult                = 2.1; 
sigmaOnf            = 0.55;
k                   = 8;
cutOff              = 0.15;
g                   = 15;               
noiseMethod         = -2;
imf2 = PhaseNeuritenessFilter2D(im, nscale, norient, minWaveLength, mult, sigmaOnf, k, cutOff, g, noiseMethod);
imf2 = Normalize(imf2);
imwrite(imf2,sprintf('imf2_%d_%d.png',mag,dir))

%% Zana's Top-hats
imsum = zana_tophat(im,12,12);
imsum = Normalize(imsum);
imwrite(imsum,sprintf('SoTH_%d_%d.png',mag,dir))

%% CLAHE
CLAHE = adapthisteq(im);
CLAHE = Normalize(CLAHE);
imwrite(CLAHE,sprintf('CLAHE_%d_%d.png',mag,dir))

%% Wavelet 
    levels = [2,3];
    imw = iuwt_vessels(im,levels);
    imw = Normalize(imw);
%     imwrite(imw,sprintf('wavelet_%d_%d.png',mag,dir))
%      imwrite(imw,strcat(name,'wavelet','.png'));
    %% B-Cosfire
    
    mask = imread('maskim0077.ppm');
    W = 15; step = 2;
    imbcosfire = im_seg(im,mask,W,step);
    imbcosfire = Normalize(imbcosfire);
    imwrite(imbcosfire,strcat(name,'bcosfire','.png'));
    
%% Plot
figure,
subplot(241), imshow(im);
subplot(242), imshow(imda);
subplot(243), imshow(Vmax);
subplot(244), imshow(imf);
subplot(245), imshow(imP);
subplot(246), imshow(imf2);
subplot(247), imshow(S_sum);
subplot(248), imshow(CLAHE);

%%
% Calculate the AUCs
AUCOrig = roc([im(:),gt(:)]);
AUC = roc([imda(:),gt(:)]);
Vmax = round(Vmax,3);
AUCVes = roc([Vmax(:),gt(:)]);
imf = round(imf,3);
AUCNe = roc([imf(:),gt(:)]);
imP = round(imP,3);
AUCPctV = roc([imP(:),gt(:)]);
imf2 = round(imf2,3);
AUCPctN = roc([imf2(:),gt(:)]);
S_sum = round(S_sum,3);
AUCSsum = roc([S_sum(:),gt(:)]);
CLAHE = round(CLAHE,3);
AUCHE = roc([CLAHE(:),gt(:)]);

%Assign Data
disp([AUCOrig.AUC,AUC.AUC,AUCVes.AUC,AUCNe.AUC,AUCPctV.AUC,AUCPctN.AUC,AUCSsum.AUC,AUCHE.AUC])
