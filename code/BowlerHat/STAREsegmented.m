clc; clear; close all;

images = {'0001','0002','0003','0004','0005','0044','0077','0081','0082','0139','0162','0163','0235','0236','0239','0240','0255','0291','0319','0324'};
i = 12;
%%
for i=1:length(images)
    %% Load Image
    im = imread(strcat('im',images{i},'.ppm'));
    gt = logical(imread(strcat('im',images{i},'.ah.ppm')));
    im = mat2gray(imcomplement(im(:,:,2)));
    
%     mask = imread(strcat('maskim',images{i},'.ppm'));
%     mask = im2bw(mask);

    % Granulometry (morphological)
    si = 15; no=10;
    [imda,~] = Granulo2D(im,si,no);
    imda= Normalize(imda);
    
%     maskimda = imerode(mask, strel('disk', 1));
%     imda = imda .* double(maskimda);

    % B-Cosfire
    
    % Symmetric filter params
    symmfilter = struct();
    symmfilter.sigma     = 2.4;
    symmfilter.len       = 8;
    symmfilter.sigma0    = 3;
    symmfilter.alpha     = 0.7;

    % Asymmetric filter params
    asymmfilter = struct();
    asymmfilter.sigma     = 1.8;
    asymmfilter.len       = 22;
    asymmfilter.sigma0    = 2;
    asymmfilter.alpha     = 0.1;
    
   [output.respimage] = BCOSFIRE_media15(im, symmfilter, asymmfilter, 0.5);
   output.segmented = (output.respimage >10);

   imbcosfire =output.segmented;
   
%    image = 1 - imda;
   [output.respimage] = BCOSFIRE_media15(imda, symmfilter, asymmfilter, 0.5);
   output.segmented = (output.respimage > 25);

   imbcosfireEnhanced =output.segmented;
   imbcosfire = Normalize(imbcosfire);
   imbcosfireEnhanced=Normalize(imbcosfireEnhanced);
    
   figure,
   subplot(141);imagesc(imda); colormap(gray); axis off; axis image; title('enhanced');
   subplot(142);imagesc(imbcosfire); colormap(gray); axis off; axis image; title('B-COSFIRE ');
   subplot(143);imagesc(imbcosfireEnhanced); colormap(gray); axis off; axis image; title('B-COSFIRE enhanced');
   subplot(144);imagesc(gt); colormap(gray); axis off; axis image; title('GT');
   %% Calculate the Individual AUC 
    
    [xrocO,yrocO,~,AUCO]          = perfcurve(gt(:),im(:),1);
    
    [xrocP,yrocP,~,AUCP]          = perfcurve(gt(:),imda(:),1);
    
    [xrocB,yrocB,~,AUCB]          = perfcurve(gt(:),imbcosfire(:),1);
   
    [xrocBen,yrocBen,~,AUCBen]    = perfcurve(gt(:),imbcosfireEnhanced(:),1);
    

    fprintf('%d\t%.3f\t%.3f\t%.3f\t%.3f\t\t\n',i,AUCO,AUCP,AUCB,AUCBen)
  
end