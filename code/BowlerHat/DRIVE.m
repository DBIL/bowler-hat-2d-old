clc; clear ; close all;

ground      = [];
raw         = [];
proposed    = [];
vesselness  = [];
neuriteness = [];
pctvessel   = [];
pctneurite  = [];
clahe       = [];
soth        = [];
wavelet     = [];
bcosfire    = [];
volumer     = [];

for i=21:40
    im = imread(strcat(num2str(i),'_training.tif'));
    gt = logical(imread(strcat(num2str(i),'_manual1.gif')));
    im = mat2gray(imcomplement(im(:,:,2)));
    raw((end+1):(end+length(im(:)))) = im(:);
    ground((end+1):(end+length(gt(:)))) = gt(:);
    mask = imread(strcat(num2str(i),'_training_mask.gif'));
    mask = im2bw(mask);
    
    %% Granulometry (morphological)
    si = 15; no=10;
    [imda,~] = Granulo2D(im,si,no);
    imda= Normalize(imda);
    imda = adapthisteq(imda,'Range','full');
    imda = imadjust(imda);
    maskimda = imerode(mask, strel('disk', 1));
    imda = imda .* double(maskimda);
    
    proposed((end+1):(end+length(imda(:)))) = imda(:);

    %% Calculate Vesselness
    sigma = 1:1:7; gamma = 2; beta = 70; c = 15; wb = false;
    [V,Vmax] = Vesselness2D(im,sigma,gamma,beta,c,wb);
    Vmax = Normalize(Vmax);
    maskVmax = imerode(mask, strel('disk', 5));
    Vmax = Vmax .* double(maskVmax);
    vesselness((end+1):(end+length(Vmax(:)))) = Vmax(:);

    %% Neutriness
    sigma = 4;
    [imf,~,~] = NeuriteneesFilter2D(im,sigma);
    imf= Normalize(imf);
    maskimf = imerode(mask, strel('disk', 5));
    imf = imf .* double(maskimf);
    neuriteness((end+1):(end+length(imf(:)))) = imf(:);
%     
    %% Volume Ratio
    sigma = 1:3;
    t = 0.5;
    RVR = vesselness2D(im, sigma, [1;1], t, false);
    RVR = Normalize(RVR);
    maskRVR = imerode(mask, strel('disk', 5));
    RVR = RVR .* double(maskRVR);
    volumer((end+1):(end+length(RVR(:)))) = RVR(:);
        
    %% PCT Vesselness 
    % Parameters
    nscale              = 6; 
    norient             = 5;
    minWaveLength       = 2;
    mult                = 2; 
    sigmaOnf            = 0.45;
    k                   = 6;
    cutOff              = 0.5;
    g                   = 5;               
    noiseMethod         = -1;
    beta                = 15; 
    c                   = 15;  

    [imP,Vx,Vy] = PhaseVesselnessFilter2D(beta,c,im,nscale,norient,minWaveLength,mult,sigmaOnf,k,...
        cutOff, g, noiseMethod);
    imP = Normalize(imP);
    maskimP = imerode(mask, strel('disk', 5));
    imP = imP .* double(maskimP);
    clear nscale norient  minWaveLength mult sigmaOnf k noiseMethod;
    pctvessel((end+1):(end+length(imP(:)))) = imP(:);

    %% PCT NEuriteness 
    nscale              = 4;  
    norient             = 6; 
    minWaveLength       = 7; 
    mult                = 2.4;  
    sigmaOnf            = 0.75; 
    k                   = 15; 
    cutOff              = 0.4; 
    g                   = 5;                
    noiseMethod         = 0; 
    [imf2,L1,L2,Lmin] = PhaseNeuritenessFilter2D(im, nscale, norient, minWaveLength, mult,... 
        sigmaOnf, k, cutOff, g, noiseMethod); 
    imf2 = Normalize(imf2);
    maskimf2 = imerode(mask, strel('disk', 5));
    imf2 = imf2 .* double(maskimf2);
    pctneurite((end+1):(end+length(imf2(:)))) = imf2(:);

    %% CLAHE
    he = adapthisteq(im);
    he = Normalize(he);
    maskhe = imerode(mask, strel('disk', 5));
    he = he .* double(maskhe);
    clahe((end+1):(end+length(he(:)))) = he(:);
    
    %% Zana's Top-hats
    imsum = zana_tophat(im,10,12);
    imsum = Normalize(imsum);
    maskimsum = imerode(mask, strel('disk', 1));
    imsum = imsum .* double(maskimsum);
      
    %% Wavelet 
    levels = [2,3];
    imw = iuwt_vessels(im,levels);
    imw = Normalize(imw);
     maskimw = imerode(mask, strel('disk', 5));
    imw = imw.* double(maskimw);
    wavelet((end+1):(end+length(imw(:)))) = imw(:);
    
    %% Line Detector
    I = imread(strcat(num2str(i),'_training.tif'));
    mask = imread(strcat(num2str(i),'_training_mask.gif'));
    W = 15; step = 2;
    imbcosfire = im_seg(I,mask,W,step);
    imbcosfire = Normalize(imbcosfire);
    bcosfire((end+1):(end+length(imbcosfire(:)))) = imbcosfire(:);
    
    %% Calculate the Individual AUC 
    rn = 3;
    im = round(im,rn);
    [xrocO,yrocO,~,AUCO]          = perfcurve(gt(:),im(:),1);
    imda = round(imda,rn);
    [xrocP,yrocP,~,AUCP]          = perfcurve(gt(:),imda(:),1);
    Vmax = round(Vmax,rn);
    [xrocV,yrocV,~,AUCV]          = perfcurve(gt(:),Vmax(:),1);
    imf = round(imf,rn);
    [xrocN,yrocN,~,AUCN]          = perfcurve(gt(:),imf(:),1);
    imP = round(imP,rn);
    [xrocPV,yrocPV,~,AUCPV]       = perfcurve(gt(:),imP(:),1);
    imf2 = round(imf2,rn);
    [xrocPN,yrocPN,~,AUCPN]       = perfcurve(gt(:),imf2(:),1);
    imsum = round(imsum,rn);
    [xrocSsum,yrocSsum,~,AUCSsum] = perfcurve(gt(:),imsum(:),1);
    he = round(he,rn);he = round(he,rn);
    [xrocHE,yrocHE,~,AUCHE]       = perfcurve(gt(:),he(:),1);
    RVR = round(RVR,rn);
    [xrocVR,yrocVR,~,AUCVR]       = perfcurve(gt(:),RVR(:),1);
    imw = round(imw,rn);
    [xrocW,yrocW,~,AUCW]          = perfcurve(gt(:),imw(:),1);
    imbcosfire = round(imbcosfire,rn);
    [xrocB,yrocB,~,AUCB]          = perfcurve(gt(:),imbcosfire(:),1);
     
    fprintf('%.3f\t%.3f\t%.3f\t%.3f\t%.3f\t%.3f\t%.3f\t%.3f\t%.3f\t%.3f\n%.3f\t%.3f\t\n',i,AUCO,AUCP,AUCV,AUCN,AUCPV,AUCPN,AUCSsum,AUCHE,AUCVR,AUCW,AUCB)
%     figure; hold on;
%     plot(xrocO,yrocO,'k','LineWidth',1.5);
%     plot(xrocP,yrocP,'LineWidth',3);
%     plot(xrocV,yrocV,'LineWidth',1.5);
%     plot(xrocN,yrocN,'LineWidth',1.5);
%     plot(xrocPV,yrocPV,'LineWidth',1.5);
%     plot(xrocPN,yrocPN,'LineWidth',1.5);
%     plot(xrocSsum,yrocSsum,'LineWidth',1.5);
%     plot(xrocHE,yrocHE,'LineWidth',1.5);
% 
%     hold off
%     xlabel('False positive rate (1-Specificity)')
%     ylabel('True positive rate (Sensitivity)')
%     axis square
%     legend('Raw','Proposed','Vesselness','Neutriness'...
% %         ,'PCT Vesselness','PCT Neuriteness','Sum of Tophats','CLAHE','Location','southeast','Orientation','horizontal');

    %% Save example for plot
    if i==40
        sz = max([length(xrocO),length(xrocP),length(xrocV),length(xrocN),...
            length(xrocPV),length(xrocPN),length(xrocSsum),length(xrocHE),length(xrocVR),length(xrocW),length(xrocB)]);
        
        results = zeros(sz,22); 
        results(1:length(xrocO),1:2)      = [xrocO,yrocO];
        results(1:length(xrocP),3:4)      = [xrocP,yrocP];
        results(1:length(xrocV),5:6)      = [xrocV,yrocV];
        results(1:length(xrocN),7:8)      = [xrocN,yrocN];
        results(1:length(xrocPV),9:10)    = [xrocPV,yrocPV];
        results(1:length(xrocPN),11:12)   = [xrocPN,yrocPN];
        results(1:length(xrocSsum),13:14) = [xrocSsum,yrocSsum];
        results(1:length(xrocHE),15:16)   = [xrocHE,yrocHE];
        results(1:length(xrocVR),17:18)   = [xrocVR,yrocVR];
        results(1:length(xrocW),19:20)    = [xrocW,yrocW];
        results(1:length(xrocB),21:22)    = [xrocB,yrocB];
        csvwrite('drive.dat',results);
    end
end
return
%% Calculate the Stacked AUC 

    [xrocO,yrocO,~,AUCO]          = perfcurve(ground(:),raw(:),1);
    [xrocP,yrocP,~,AUCP]          = perfcurve(ground(:),proposed(:),1);
    [xrocV,yrocV,~,AUCV]          = perfcurve(ground(:),vesselness(:),1);
    [xrocN,yrocN,~,AUCN]          = perfcurve(ground(:),neuriteness(:),1);
    [xrocPV,yrocPV,~,AUCPV]       = perfcurve(ground(:),pctvessel(:),1);
    [xrocPN,yrocPN,~,AUCPN]       = perfcurve(ground(:),pctneurite(:),1);
    [xrocSsum,yrocSsum,~,AUCSsum] = perfcurve(ground(:),soth(:),1);
    [xrocHE,yrocHE,~,AUCHE]       = perfcurve(ground(:),clahe(:),1);
    [xrocVR,yrocVR,~,AUCVR]       = perfcurve(ground(:),volumer(:),1);
    [xrocW,yrocW,~,AUCW]          = perfcurve(ground(:),wavelet(:),1);
    [xrocB,yrocB,~,AUCB]          = perfcurve(ground(:),bcosfire(:),1);
    disp([AUCO,AUCP,AUCV,AUCN,AUCPV,AUCPN,AUCSsum,AUCHE,AUCVR,AUCW,AUCB])
% 
%% display graph
H = figure; hold on;
plot(xrocO,yrocO,'k','LineWidth',1.5);
plot(xrocP,yrocP,'LineWidth',3);
plot(xrocV,yrocV,'LineWidth',1.5);
plot(xrocN,yrocN,'LineWidth',1.5);
plot(xrocPV,yrocPV,'LineWidth',1.5);
plot(xrocPN,yrocPN,'LineWidth',1.5);
plot(xrocSsum,yrocSsum,'LineWidth',1.5);
plot(xrocHE,yrocHE,'LineWidth',1.5);
plot(xrocVR,yrocVR,'LineWidth',1.5);
plot(xrocW,yrocW,'LineWidth',1.5);
plot(xrocB,yrocB,'LineWidth',1.5);

hold off
xlabel('False positive rate (1-Specificity)')
ylabel('True positive rate (Sensitivity)')
axis square
legend('Raw','Proposed','Vesselness','Neutriness'...
    ,'PCT Vesselness','PCT Neuriteness','Sum of Tophats','CLAHE','Regularised VR','Wavelet','Bcosfire','Location','southeast','Orientation','horizontal');
