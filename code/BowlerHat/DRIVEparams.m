clc; clear; %close all;

i=21;
rn = 3;
results = zeros(100,45);
    for si=5:25
        for no=6:20
            im = imread(strcat(num2str(i),'_training.tif'));
            gt = logical(imread(strcat(num2str(i),'_manual1.gif')));
            im = mat2gray(imcomplement(im(:,:,2)));
            mask = imread(strcat(num2str(i),'_training_mask.gif'));
            mask = im2bw(mask);
            %% Granulometry (morphological)
            [imda,~] = Granulo2D(im,16,10);
            imda = Normalize(imda);    
            %% Calculate the AUC 
            maskimda = imerode(mask, strel('disk', 2));
            imda = imda .* double(mask);
            [xrocP,yrocP,~,AUCP] = perfcurve(gt(:),imda(:),1);
            disp([si no AUCP]);
            results(si,no) = AUCP;
        end   
    end
    [x,y] = mean(results==max(results(:)));
    disp(i);
%     
% Plot
figure; plot(x,y);
