clc; clear all; close all;
rng(42)

%% Loops
data = zeros(179,8);
for angle = 5:175
    fprintf('Branch with angle of %d degrees;\n',angle);
    % Create image
    gt = branch(angle);
    im = imgaussfilt(double(gt));

    % Proposed Method
    si = 40; no=20;
    imda = Granulo2D(im,si,no);
    imda= Normalize(imda);

    % Vesselness
    sigma = 0.2:0.5:6; gamma = 2; beta = 0.5; c = 15; wb = false;
    [~,Vmax] = Vesselness2D(im,sigma,gamma,beta,c,wb);
    Vmax = Normalize(Vmax);

    % Neutriness
    sigma = 6;
    [imf,~,~] = NeuriteneesFilter2D(im,sigma);
    imf= Normalize(imf);

    % PCT Vesselness 
    nscale              = 10; 
    norient             = 3;
    minWaveLength       = 2;
    mult                = 2; 
    sigmaOnf            = 0.45;
    k                   = 5;
    cutOff              = 0.5;
    g                   = 5;               
    noiseMethod         = -1;
    beta = 20;c = 15;%Vesselness parameters
    imP = PhaseVesselnessFilter2D(beta,c,im,nscale,norient,minWaveLength,mult,sigmaOnf,k,cutOff, g, noiseMethod);
    imP = Normalize(imP);
    clear nscale norient  minWaveLength mult sigmaOnf k noiseMethod;

    % PCT NEuriteness 
    nscale              = 6; 
    norient             = 4;
    minWaveLength       = 3;
    mult                = 3; 
    sigmaOnf            = 0.55;
    k                   = 6;
    cutOff              = 0.25;
    g                   = 15;               
    noiseMethod         = -1;
    imf2 = PhaseNeuritenessFilter2D(im, nscale, norient, minWaveLength, mult, sigmaOnf, k, cutOff, g, noiseMethod);
    imf2 = Normalize(imf2);

    % Sum of Top-hats
    S_sum = tophatenhancement(im);
    S_sum = Normalize(S_sum);

    % CLAHE
    CLAHE = adapthisteq(im);
    CLAHE = Normalize(CLAHE);

    % Calculate the AUCs
    AUCOrig = roc([im(:),gt(:)]);
    AUC = roc([imda(:),gt(:)]);
    try
    Vmax = round(Vmax,3);
    AUCVes = roc([Vmax(:),gt(:)]);
    catch
        AUCVes.AUC=0;
    end
    try
    imf = round(imf,3);
    AUCNe = roc([imf(:),gt(:)]);
    catch
        AUCNe.AUC=0;
    end
    try
    imP = round(imP,3);
    AUCPctV = roc([imP(:),gt(:)]);
    catch
        AUCPctV.AUC=0;
    end
    try
    imf2 = round(imf2,3);
    AUCPctN = roc([imf2(:),gt(:)]);
    catch
        AUCPctN.AUC=0;
    end
    try
    S_sum = round(S_sum,3);
    AUCSsum = roc([S_sum(:),gt(:)]);
    catch
        AUCSsum.AUC=0;
    end
    try
    CLAHE = round(CLAHE,3);
    AUCHE = roc([CLAHE(:),gt(:)]);
    catch
        AUCHE.AUC=0;
    end

    %Assign Data
    data(angle,1) = AUCOrig.AUC;
    data(angle,2) = AUC.AUC;
    data(angle,3) = AUCVes.AUC;
    data(angle,4) = AUCNe.AUC;
    data(angle,5) = AUCPctV.AUC;
    data(angle,6) = AUCPctN.AUC;
    data(angle,7) = AUCSsum.AUC;
    data(angle,8) = AUCHE.AUC;
        
end

%% Save data
csvwrite('angle.dat',data);
% return

%% Plot
figure; hold on;%('Units','Normalized','Position',[0,0,1,1]); hold on;
hold on;
for method = 1:8
    plot(1:size(data,1),squeeze(data(:,method)),'LineWidth',2);
end
xlabel('Angle (degrees)'); ylabel('Area Under ROC Curve (Normalised)');
legend('Raw','Proposed','Vesselness','Neutriness','PCT Vesselness','PCT Neuriteness','Sum of TopHats','CLAHE','Location','southoutside','Orientation','horizontal');