%% Clear
clc; clear; %close all;
%% Create Synthetic
im = Synthetic('cross','gaussian');     % plane im = Synthetic(type,noise); type => curve , blob, line,  cross, plane
                                    % noise => salt, gaussian, no.
%% Load Image
% im = imread ('leaf2.tif');    % real data call
% im = im(:,:,2); % green plane image
%% Normalize
% im = rgb2gray(im);                   % use when the data is rgb format
% im = imcomplement(im);               % use when the background is white
                                       % and vessels are black
im = Normalize(im);
%% Granulometry
si = 30;                              % size of the disk
no = 20;                              % number of orientation
[diff,~] = Granulo2D(im,si,no);     % call the granulometry scriptben
%% Plot
figure; 
subplot(121); imagesc(im); colormap jet; axis off; axis equal; axis tight; title('original');
subplot(122); imagesc(diff); colormap jet; axis off; axis equal; axis tight; title('diff');
figure;
subplot(121); imagesc(im); colormap gray; axis off; axis equal; axis tight; title('original');
subplot(122); imagesc(diff); colormap gray; axis off; axis equal; axis tight; title('diff');
return
%% save result
imwrite(diff,'blobVesGranula.png');