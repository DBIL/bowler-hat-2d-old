clc; clear ; close all;

%% Synyhetic Image Create
%  name = 'blobVes';
%  [im,gt]=Synthetic(name,'gaussian'); % bend, curve , blob, line, cross, plane, blobVes
%% Read real image
file='35_drive_crop.tif';
im=imread(file);
[pathstr,name,ext] = fileparts(file);
im = im(:,:,2);
im = mat2gray(im);
% imwrite(im,strcat(name,'.png'));
im = imcomplement(im);%needed for retinal images only
% Ip = single(im);
% thr = prctile(Ip(Ip(:)>0),1) * 0.9;
% Ip(Ip<=thr) = thr;
% Ip = Ip - min(Ip(:));
% im = Ip ./ max(Ip(:));    
%% GROUND TRUTH
% gt = imread('');
% imwrite(im,strcat(name,'.png'));

%% Granulometry (morphological)
si = 20; no=20;
[imda,imra] = Granulo2D(im,si,no);
imda= Normalize(imda);
imwrite(imda,strcat(name,'Granulo.png'));

%% Calculate Vesselness
im = adapthisteq(im,'Range','full');
im = imadjust(im);
sigma = 1:2; gamma = 3; beta = 0.5; c = 15; wb = true;
[V,Vmax] = Vesselness2D(imcomplement(im),sigma,gamma,beta,c,wb);
Vmax = Normalize(Vmax);
imwrite(Vmax,strcat(name,'Vessel','.png'));
% figure,imagesc(Vmax),colormap gray, axis off; axis equal; axis tight;

%% Neutriness
sigma = 3;
[imf,~,~] = NeuriteneesFilter2D(im,sigma);
imf= Normalize(imf);
imwrite(imf,strcat(name,'Neurite','.png'));

%% Volume Ratio
% RVR = volumeratio(im,0.5:0.5:2.5,2,0.9);
RVR = vesselness2D(im, 0.5:0.5:2.5, [1;1], 0.9, true);
RVR = Normalize(RVR);
imwrite(RVR,strcat(name,'VR','.png'));

%% PCT Vesselness 
% Parameters
% im = adapthisteq(im,'Range','full');
% im = imadjust(im);
nscale              = 6; 
norient             = 10;
minWaveLength       = 6;
mult                = 2; 
sigmaOnf            = 0.35;
k                   = 5;
cutOff              = 0.7;
g                   = 5;               
noiseMethod         = -1;
beta                = 0.5; 
c                   = 15;  

[imP,Vx,Vy] = PhaseVesselnessFilter2D(beta,c,imcomplement(im),nscale,norient,minWaveLength,mult,sigmaOnf,k,...
    cutOff, g, noiseMethod);
imP = Normalize(imP);
imwrite(imP,strcat(name,'PCTV','.png'));
clear nscale norient  minWaveLength mult sigmaOnf k noiseMethod;

%% PCT NEuriteness 
nscale              = 5;
norient             = 6;
minWaveLength       = 4;
mult                = 3; 
sigmaOnf            = 0.35;
k                   = 6;
cutOff              = 0.7;
g                   = 5;               
noiseMethod         = -1;
[imf2,L1,L2,Lmin] = PhaseNeuritenessFilter2D(im, nscale, norient, minWaveLength, mult,...
    sigmaOnf, k, cutOff, g, noiseMethod);
imf2 = Normalize(imf2);
imwrite(imf2,strcat(name,'PCTN','.png'));

%% Zana's Top-hats
imsum = zana_tophat(im,10,12);
imsum = Normalize(imsum);
imwrite(imsum,strcat(name,'SoTH','.png'));

%% CLAHE
CLAHE = adapthisteq((im));
CLAHE = Normalize(CLAHE);
imwrite(CLAHE,strcat(name,'CLAHE','.png'));

%% Wavelet 
    levels = [2,3];
    imw = iuwt_vessels((im),levels);
%     imw = Normalize(imw);
%     imwrite(imw,strcat(name,'wavelet','.png'));
    
    %% Line Detector
    I = imread('crop_1_h.jpg');
    mask = ones(162,174);
    W = 15; step = 2;
    imbcosfire = im_seg(I,mask,W,step);
    imbcosfire = Normalize(imbcosfire);
    imwrite(imbcosfire,strcat(name,'bcosfire','.png'));
    return
%% Calculate the AUC 
[AUCOrig,xfit,yfit,xrocO,yrocO]= roc([im(:),gt(:)]); Orig = AUCOrig.AUC;
imda = round(imda,3);
[AUC,~,~,xroc,yroc] = roc([imda(:),gt(:)]); auc  = AUC.AUC;
Vmax = round(Vmax,3);
[AUCVes,~,~,xrocV,yrocV] = roc([Vmax(:),gt(:)]);  Ves  = AUCVes.AUC;
imf = round(imf,3);
[AUCNe,~,~,xrocN,yrocN] = roc([imf(:),gt(:)]);         Ne   = AUCNe.AUC;
imP = round(imP,3);
[AUCPctV,~,~,xrocPV,yrocPV] = roc([imP(:),gt(:)]);     PctV = AUCPctV.AUC;
imf2 = round(imf2,3);
[AUCPctN,~,~,xrocPN,yrocPN] = roc([imf2(:),gt(:)]);    PctN = AUCPctN.AUC;
S_sum = round(S_sum,3);
[AUCSsum,~,~,xrocSsum,yrocSsum] = roc([S_sum(:),gt(:)]);    Ssum = AUCSsum.AUC;
CLAHE = round(CLAHE,3);
[AUCHE,~,~,xrocHE,yrocHE] = roc([CLAHE(:),gt(:)]);    HE= AUCHE.AUC;
RVR = round(RVR,3);
[AUCVR,~,~,xrocVR,yrocVR] = roc([RVR(:),gt(:)]);    VR = AUCVR.AUC;

%% display graph
H=figure;
set(H,'Position',[4 402 560 420])
hold on
% plot([0 1],[0 1],'k');
plot(xfit,yfit,'LineWidth',1.5);
%set(H1,'markersize',4,'color','g')
H2=plot(xroc,yroc,'LineWidth',1.5);
%set(H2,'markersize',4,'color','b')
H3=plot(xrocV,yrocV,'LineWidth',1.5);
%set(H3,'markersize',4,'color','c')
H4=plot(xrocN,yrocN,'LineWidth',1.5);
% set(H4,'markersize',4,'color','m')
H5=plot(xrocPV,yrocPV,'LineWidth',1.5);
% set(H5,'markersize',4,'color','y')
H6=plot(xrocPN,yrocPN,'LineWidth',1.5);
% set(H6,'markersize',7,'color','k')
H7=plot(xrocSsum,yrocSsum,'LineWidth',1.5);
% set(H7,'markersize',7,'color','k')
H8=plot(xrocHE,yrocHE,'LineWidth',1.5);
% set(H8,'markersize',7,'color','k')
H9=plot(xrocVR,yrocVR,'LineWidth',1.5);
% % set(H8,'markersize',7,'color','k')

hold off
xlabel('False positive rate (1-Specificity)')
ylabel('True positive rate (Sensitivity)')
axis square
legend('ROC','Proposed','Vesselness','Neutriness'...
    ,'PCT Vesselness','PCT Neuriteness','Sum of Tophats','CLAHE','Regularised VR','Location','southeast','Orientation','horizontal');
print(H,'ROCurvePlot','-dpng')

%%
figure,
subplot(331),imagesc(im); colormap gray; axis off; axis equal; axis tight;title('Raw');
subplot(332),imagesc(imda);colormap gray; axis off; axis equal; axis tight;title('Proposed');
subplot(333),imagesc(Vmax);colormap gray; axis off; axis equal; axis tight;title('Vesselness');
subplot(334),imagesc(imf);colormap gray; axis off; axis equal; axis tight;title('Neuritness');
subplot(335),imagesc(imP);colormap gray; axis off; axis equal; axis tight;title('PCT Ves');
subplot(336),imagesc(imf2);colormap gray; axis off; axis equal; axis tight;title('PCT Ne');
subplot(337),imagesc(S_sum);colormap gray; axis off; axis equal; axis tight;title('Sum of Tophats');
subplot(338),imagesc(CLAHE);colormap gray; axis off; axis equal; axis tight;title('CLAHE');
subplot(339),imagesc(RVR);colormap gray; axis off; axis equal; axis tight;title('Regularised VR');

%%
% fprintf (' Original      :%0.5f \n Proposed      :%0.5f\n Vesselness    :%0.5f\n Neuriteness   :%0.5f\n PCT Ves       :%0.5f\n PCT Ne        :%0.5f\n'...
%       ,Orig,auc,Ves,Ne,PctV,PctN);
disp([Orig,auc,Ves,Ne,PctV,PctN,Ssum,HE,VR])