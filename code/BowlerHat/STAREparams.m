clc; clear all; %close all;

% images = {'0001','0002','0003','0004','0005','0044','0077','0081','0082','0139','0162','0163','0235','0236','0239','0240','0255','0291','0319','0324'};
i='0004';
im = imread(strcat('im',i,'.ppm'));
gt = logical(imread(strcat('im',i,'.vk.ppm')));
im = mat2gray(imcomplement(im(:,:,2)));

rn = 3;
results = zeros(25,15);
for si=2:25
    for no=2:15
        %% Granulometry (morphological)
        [imda,~] = Granulo2D(im,si,no);
        imda = Normalize(imda);
        
        %% Calculate the AUC 
        imda = round(imda,rn);
        [AUCP,~,~,xrocP,yrocP] = roc([imda(:),gt(:)]);
        
        results(si,no) = AUCP.AUC;
    end   
end
    
%% Plot
figure; imagesc(results);
[x,y] = find(results==max(results(:)));
disp([x,y]);
