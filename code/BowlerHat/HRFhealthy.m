clc; clear ; close all;

ground      = [];
raw         = [];
proposed    = [];
vesselness  = [];
neuriteness = [];
pctvessel   = [];
pctneurite  = [];
clahe       = [];
soth        = [];
wavelet     = [];
bcosfire    = [];
volumer     = [];

for i=1:15
    im = imread(strcat(num2str(i),'_h.jpg'));
    gt = logical(imread(strcat(num2str(i),'_h.tif')));
    im = mat2gray(imcomplement(im(:,:,2)));
    raw((end+1):(end+length(im(:)))) = im(:);
    ground((end+1):(end+length(gt(:)))) = gt(:);
    mask = imread(strcat(num2str(i),'_h_mask.tif'));
    mask = im2bw(mask);

    %% Granulometry (morphological)
    tic
    si = 28; no=12;
    [imda,~] = Granulo2D(im,si,no);
    toc
    imda= Normalize(imda);
    proposed((end+1):(end+length(imda(:)))) = imda(:);

    %% Calculate Vesselness
    sigma = 1:1:7; gamma = 2; beta = 70; c = 15; wb = false;
    [V,Vmax] = Vesselness2D(im,sigma,gamma,beta,c,wb);
    Vmax = Normalize(Vmax);
    vesselness((end+1):(end+length(Vmax(:)))) = Vmax(:);

    %% Neutriness
    sigma = 4;
    [imf,~,~] = NeuriteneesFilter2D(im,sigma);
    imf= Normalize(imf);
    neuriteness((end+1):(end+length(imf(:)))) = imf(:);
    
    %% Volume Ratio
    sigma = 1:3;
    gamma = 2;
    t = 0.9;
    RVR = volumeratio(im,sigma,gamma,t);
    RVR = Normalize(RVR);
    volumer((end+1):(end+length(RVR(:)))) = RVR(:);
        
    %% PCT Vesselness 
    % Parameters
    nscale              = 6; 
    norient             = 5;
    minWaveLength       = 2;
    mult                = 2; 
    sigmaOnf            = 0.45;
    k                   = 6;
    cutOff              = 0.5;
    g                   = 5;               
    noiseMethod         = -1;
    beta                = 15; 
    c                   = 15;  

    [imP,Vx,Vy] = PhaseVesselnessFilter2D(beta,c,im,nscale,norient,minWaveLength,mult,sigmaOnf,k,...
        cutOff, g, noiseMethod);
    imP = Normalize(imP);
    clear nscale norient  minWaveLength mult sigmaOnf k noiseMethod;
    pctvessel((end+1):(end+length(imP(:)))) = imP(:);

    %% PCT NEuriteness 
    nscale              = 3;  
    norient             = 6; 
    minWaveLength       = 7; 
    mult                = 2.4;  
    sigmaOnf            = 0.75; 
    k                   = 15; 
    cutOff              = 0.4; 
    g                   = 5;                
    noiseMethod         = -1; 
    [imf2,L1,L2,Lmin] = PhaseNeuritenessFilter2D(im, nscale, norient, minWaveLength, mult,... 
        sigmaOnf, k, cutOff, g, noiseMethod); 
    imf2 = Normalize(imf2);
    pctneurite((end+1):(end+length(imf2(:)))) = imf2(:);

    %% CLAHE
    he = adapthisteq(imcomplement(im));
    he = Normalize(he);
    clahe((end+1):(end+length(he(:)))) = he(:);
    
     %% Zana's Top-hats
      imsum = zana_tophat(im,25,12);
      imsum = Normalize(imsum);
      maskimsum = imerode(mask, strel('disk', 1));
      imsum = imsum .* double(maskimsum);
    
    %% Wavelet 
    levels = [2,3];
    imw = iuwt_vessels(im,levels);
    imw = Normalize(imw);
    wavelet((end+1):(end+length(imw(:)))) = imw(:);
    
    %% Line Detector
    I = imread(strcat(num2str(i),'_h.jpg'));
    mask = imread(strcat(num2str(i),'_h_mask.tif'));
    W = 15; step = 2;
    imbcosfire = im_seg(I,mask,W,step);
    imbcosfire = Normalize(imbcosfire);
    bcosfire((end+1):(end+length(imbcosfire(:)))) = imbcosfire(:);
    

    %% Calculate the Individual AUC 
    [xrocO,yrocO,~,AUCO]          = perfcurve(gt(:),im(:),1);
    [xrocP,yrocP,~,AUCP]          = perfcurve(gt(:),imda(:),1);
    [xrocV,yrocV,~,AUCV]          = perfcurve(gt(:),Vmax(:),1);
    [xrocN,yrocN,~,AUCN]          = perfcurve(gt(:),imf(:),1);
    [xrocPV,yrocPV,~,AUCPV]       = perfcurve(gt(:),imP(:),1);
    [xrocPN,yrocPN,~,AUCPN]       = perfcurve(gt(:),imf2(:),1);
    [xrocSsum,yrocSsum,~,AUCSsum] = perfcurve(gt(:),imsum(:),1);
    [xrocHE,yrocHE,~,AUCHE]       = perfcurve(gt(:),he(:),1);
    [xrocVR,yrocVR,~,AUCVR]       = perfcurve(gt(:),RVR(:),1);
    [xrocW,yrocW,~,AUCW]          = perfcurve(gt(:),imw(:),1);
    [xrocB,yrocB,~,AUCB]          = perfcurve(gt(:),imbcosfire(:),1);

%     fprintf('%.3f\t%.3f\t%.3f\t%.3f\t%.3f\t%.3f\t%.3f\t%.3f\t%.3f\n%.3f\t%.3f\t\n',AUCO,AUCP,AUCV,AUCN,AUCPV,AUCPN,AUCSsum,AUCHE,AUCVR,AUCW,AUCB)

%     figure; hold on;
%     plot(xrocO,yrocO,'k','LineWidth',1.5);
%     plot(xrocP,yrocP,'LineWidth',3);
%     plot(xrocV,yrocV,'LineWidth',1.5);
%     plot(xrocN,yrocN,'LineWidth',1.5);
%     plot(xrocPV,yrocPV,'LineWidth',1.5);
%     plot(xrocPN,yrocPN,'LineWidth',1.5);
%     plot(xrocSsum,yrocSsum,'LineWidth',1.5);
%     plot(xrocHE,yrocHE,'LineWidth',1.5);
% 
%     hold off
%     xlabel('False positive rate (1-Specificity)')
%     ylabel('True positive rate (Sensitivity)')
%     axis square
%     legend('Raw','Proposed','Vesselness','Neutriness'...
%         ,'PCT Vesselness','PCT Neuriteness','Sum of Tophats','CLAHE','Location','southeast','Orientation','horizontal');

    %% Save example for plot
    if i==3
%         sz = max([length(xrocO),length(xrocP),length(xrocV),length(xrocN),...
%             length(xrocPV),length(xrocPN),length(xrocSsum),length(xrocHE),length(xrocVR),length(xrocW),length(xrocB)]);
%         
%         results = zeros(sz,22);
%         results(1:length(xrocO),1:2)      = [xrocO,yrocO];
%         results(1:length(xrocP),3:4)      = [xrocP,yrocP];
%         results(1:length(xrocV),5:6)      = [xrocV,yrocV];
%         results(1:length(xrocN),7:8)      = [xrocN,yrocN];
%         results(1:length(xrocPV),9:10)    = [xrocPV,yrocPV];
%         results(1:length(xrocPN),11:12)   = [xrocPN,yrocPN];
%         results(1:length(xrocSsum),13:14) = [xrocSsum,yrocSsum];
%         results(1:length(xrocHE),15:16)   = [xrocHE,yrocHE];
%         results(1:length(xrocVR),17:18)   = [xrocVR,yrocVR];
%         results(1:length(xrocVR),19:20)   = [xrocW,yrocW];
%         results(1:length(xrocVR),21:22)   = [xrocB,yrocB];
        csvwrite('hrfSsum.dat',[xrocSsum,yrocSsum]);
    end
    
end
    return
    
% %% Calculate the Stacked AUC 
% rn = 3;
% raw = round(raw,rn);
% [AUCO,~,~,xrocO,yrocO]= roc([raw(:),ground(:)]);
% proposed = round(proposed,rn);
% [AUCP,~,~,xrocP,yrocP] = roc([proposed(:),ground(:)]);
% vesselness = round(vesselness,rn);
% [AUCV,~,~,xrocV,yrocV] = roc([vesselness(:),ground(:)]);
% neuriteness = round(neuriteness,rn);
% [AUCN,~,~,xrocN,yrocN] = roc([neuriteness(:),ground(:)]);
% pctvessel = round(pctvessel,rn);
% [AUCPV,~,~,xrocPV,yrocPV] = roc([pctvessel(:),ground(:)]);
% pctneurite = round(pctneurite,rn);
% [AUCPN,~,~,xrocPN,yrocPN] = roc([pctneurite(:),ground(:)]);
% soth = round(soth,rn);
% [AUCSsum,~,~,xrocSsum,yrocSsum] = roc([soth(:),ground(:)]);
% clahe = round(clahe,rn);
% [AUCHE,~,~,xrocHE,yrocHE] = roc([clahe(:),ground(:)]);
% 
% disp([AUCO.AUC,AUCP.AUC,AUCV.AUC,AUCN.AUC,AUCPV.AUC,AUCPN.AUC,AUCSsum.AUC,AUCHE.AUC])
% 
%% display graph
H = figure; hold on;
plot(xrocO,yrocO,'k','LineWidth',1.5);
plot(xrocP,yrocP,'LineWidth',3);
plot(xrocV,yrocV,'LineWidth',1.5);
plot(xrocN,yrocN,'LineWidth',1.5);
plot(xrocPV,yrocPV,'LineWidth',1.5);
plot(xrocPN,yrocPN,'LineWidth',1.5);
plot(xrocSsum,yrocSsum,'LineWidth',1.5);
plot(xrocHE,yrocHE,'LineWidth',1.5);
plot(xrocVR,yrocVR,'LineWidth',1.5);

hold off
xlabel('False positive rate (1-Specificity)')
ylabel('True positive rate (Sensitivity)')
axis square
legend('Raw','Proposed','Vesselness','Neutriness'...
    ,'PCT Vesselness','PCT Neuriteness','Sum of Tophats','CLAHE','Regularised VR','Location','southeast','Orientation','horizontal');
