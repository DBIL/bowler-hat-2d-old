clc; clear all; close all;

%% Image Create
[im,gt]=Synthetic('line','gaussian'); % curve , blob, line,  cross, plane
im = mat2gray(im);

%% Proposed Method
si = 20; no=16;
imda = Granulo2D(im,si,no);
imda= Normalize(imda);

%% Vesselness
im1 = imcomplement(im);
sigma = 0.2:0.5:6; gamma = 2; beta = 0.5; c = 15; wb = true;
[~,Vmax] = Vesselness2D(im1,sigma,gamma,beta,c,wb);
Vmax = Normalize(Vmax);

%% Neutriness
sigma = 6;
[imf,~,~] = NeuriteneesFilter2D(im,sigma);
imf= Normalize(imf);

%% Volume Ratio
sigma = 1:3;
t = 0.7;
RVR = vesselness2D(im, sigma, [1;1], t, true);
RVR = Normalize(RVR);

%% PCT Vesselness 
nscale              = 10; 
norient             = 3;
minWaveLength       = 2;
mult                = 2; 
sigmaOnf            = 0.45;
k                   = 5;
cutOff              = 0.5;
g                   = 5;               
noiseMethod         = -1;
beta = 20;c = 15;%Vesselness parameters
imP = PhaseVesselnessFilter2D(beta,c,im1,nscale,norient,minWaveLength,mult,sigmaOnf,k,cutOff, g, noiseMethod);
imP = Normalize(imP);
clear nscale norient  minWaveLength mult sigmaOnf k noiseMethod;

%% PCT NEuriteness 
nscale              = 6; 
norient             = 4;
minWaveLength       = 3;
mult                = 3; 
sigmaOnf            = 0.55;
k                   = 6;
cutOff              = 0.25;
g                   = 15;               
noiseMethod         = -2;
imf2 = PhaseNeuritenessFilter2D(im, nscale, norient, minWaveLength, mult, sigmaOnf, k, cutOff, g, noiseMethod);
imf2 = Normalize(imf2);

%% Zana's Top-hats
imsum = zana_tophat(im,10,12);
imsum = Normalize(imsum);

%% CLAHE
CLAHE = adapthisteq(im);
CLAHE = Normalize(CLAHE);
%% Wavelets
levels = [2,3];
imw = iuwt_vessels(im,levels);
imw = Normalize(imw); 
%% Line detector)
mask = ones(size(im));
W = 15; step = 2;
imbcosfire = im_seg(im,mask,W,step);
imbcosfire = Normalize(imbcosfire);
%% Plot Orthogonal
figure, hold on;
plot(-10:10,im(40:60,50),'LineWidth',1.5);
plot(-10:10,imda(40:60,50),'LineWidth',1.5);
plot(-10:10,Vmax(40:60,50),'LineWidth',1.5);
plot(-10:10,imf(40:60,50),'LineWidth',1.5);
plot(-10:10,imP(40:60,50),'LineWidth',1.5);
plot(-10:10,imf2(40:60,50),'LineWidth',1.5);
plot(-10:10,imsum(40:60,50),'LineWidth',1.5);
plot(-10:10,CLAHE(40:60,50),'LineWidth',1.5);
plot(-10:10,RVR(40:60,50),'LineWidth',1.5);
plot(-10:10,imw(40:60,50),'LineWidth',1.5);
plot(-10:10,imbcosfire(40:60,50),'LineWidth',1.5);
xlabel('Distance from Vessel Centreline'); ylabel('Normalised Response');
legend('Raw','Proposed','Vesselness','Neutriness','PCT Vesselness','PCT Neuriteness','Sum of TopHats','CLAHE','Regularised VR','Wavelet','Bicosfire','Location','northeastoutside');
print('profilesOrthogonal.png','-dpng','-r600');
csvwrite('profiles.dat',[   [-10:10]',...
                            im(40:60,50),...
                            imda(40:60,50),...
                            Vmax(40:60,50),...
                            imf(40:60,50),...
                            imP(40:60,50),...
                            imf2(40:60,50),...
                            imsum(40:60,50),...
                            CLAHE(40:60,50),...
                            RVR(40:60,50),...
                            imw(40:60,50),...
                            imbcosfire(40:60,50)]);


%% Plot Tangential?
figure, hold on;
plot(im(50,15:85));
plot(gt(50,15:85));
plot(imda(50,15:85));
plot(Vmax(50,15:85));
plot(imf(50,15:85));
plot(imP(50,15:85));
plot(imf2(50,15:85));
plot(imsum(50,15:85));
plot(CLAHE(50,15:85));
plot(RVR(50,15:85));
legend('Raw','Proposed','Vesselness','Neutriness','PCT Vesselness','PCT Neuriteness','Sum of TopHats','CLAHE','Regularised VR','Location','northeastoutside');
print('profilesOrthogonal.png','-dpng','-r600');


