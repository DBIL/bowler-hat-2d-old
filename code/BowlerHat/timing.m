clc; clear all; %close all;

%% Image Create
name = 'curve';
[im,~]=Synthetic(name,'no'); % bend, curve , blob, line, cross, plane, blobVes
im = rand(1000,1000);

%%
time= zeros(7,1);

%% Proposed Method
tic;
si = 40; no=20;
imda = Granulo2D(im,si,no);
imda= Normalize(imda);
time(1) = toc;

%% Vesselness
tic;
sigma = 0.2:0.5:6; gamma = 2; beta = 0.5; c = 15; wb = false;
[~,Vmax] = Vesselness2D(im,sigma,gamma,beta,c,wb);
Vmax = Normalize(Vmax);
time(2) = toc;

%% Neutriness
tic;
sigma = 6;
[imf,~,~] = NeuriteneesFilter2D(im,sigma);
imf= Normalize(imf);
time(3) = toc;

%% PCT Vesselness 
tic;
nscale              = 10; 
norient             = 3;
minWaveLength       = 2;
mult                = 2; 
sigmaOnf            = 0.45;
k                   = 5;
cutOff              = 0.5;
g                   = 5;               
noiseMethod         = -1;
beta = 20;c = 15;%Vesselness parameters
imP = PhaseVesselnessFilter2D(beta,c,im,nscale,norient,minWaveLength,mult,sigmaOnf,k,cutOff, g, noiseMethod);
imP = Normalize(imP);
clear nscale norient  minWaveLength mult sigmaOnf k noiseMethod;
time(4) = toc;

%% PCT NEuriteness 
tic;
nscale              = 6; 
norient             = 4;
minWaveLength       = 3;
mult                = 3; 
sigmaOnf            = 0.55;
k                   = 6;
cutOff              = 0.25;
g                   = 15;               
noiseMethod         = -1;
imf2 = PhaseNeuritenessFilter2D(im, nscale, norient, minWaveLength, mult, sigmaOnf, k, cutOff, g, noiseMethod);
imf2 = Normalize(imf2);
time(5) = toc;

%% Sum of Top-hats
tic;
S_sum = tophatenhancement(im);
S_sum = Normalize(S_sum);
time(6) = toc;

%% CLAHE
tic;
CLAHE = adapthisteq(im);
CLAHE = Normalize(CLAHE);
time(7) = toc;

%%
disp(time)