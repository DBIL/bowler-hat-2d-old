clc; clear all; close all;
rng(42)
%% Image Create
[imOrig,gt]=Synthetic('line','no'); % curve , blob, line,  cross, plane

%% Loops
% noisetypes = {'pepper','speckle','gaussian','multifreq'};
noisetypes = {'pepper','speckle','gaussian'};
data = zeros(3,22,11);
for noise = noisetypes
    for psnr = 4:25
        fprintf('Image with PSNR %d of %s noise;\n',psnr,noise{1});
        [im] = noiseAtPSNR(imOrig, psnr, noise{1});

        % Proposed Method
        si = 20; no=20;
        imda = Granulo2D(im,si,no);
        imda= Normalize(imda);
        
%         % Vesselness
        sigma = 0.2:0.5:6; gamma = 2; beta = 0.5; c = 15; wb = false;
        [~,Vmax] = Vesselness2D(im,sigma,gamma,beta,c,wb);
        Vmax = Normalize(Vmax);
        
        % Neutriness
        sigma = 2;
        [imf,~,~] = NeuriteneesFilter2D(im,sigma);
        imf= Normalize(imf);
        
        % Volume Ratio
        sigma = 1:3;
        gamma = 2;
        t = 0.9;
        RVR = volumeratio(im,sigma,gamma,t);
        RVR = Normalize(RVR);
        
        % PCT Vesselness 
        nscale              = 10; 
        norient             = 3;
        minWaveLength       = 2;
        mult                = 2; 
        sigmaOnf            = 0.45;
        k                   = 5;
        cutOff              = 0.5;
        g                   = 5;               
        noiseMethod         = -1;
        beta = 20;c = 15;%Vesselness parameters
        imP = PhaseVesselnessFilter2D(beta,c,im,nscale,norient,minWaveLength,mult,sigmaOnf,k,cutOff, g, noiseMethod);
        imP = Normalize(imP);
        clear nscale norient  minWaveLength mult sigmaOnf k noiseMethod;
        
        % PCT NEuriteness 
        nscale              = 6; 
        norient             = 4;
        minWaveLength       = 3;
        mult                = 3; 
        sigmaOnf            = 0.55;
        k                   = 6;
        cutOff              = 0.25;
        g                   = 15;               
        noiseMethod         = -1;
        imf2 = PhaseNeuritenessFilter2D(im, nscale, norient, minWaveLength, mult, sigmaOnf, k, cutOff, g, noiseMethod);
        imf2 = Normalize(imf2);
        
        % Zana's Top-hat
        imsum = zana_tophat(im,10,12);
        imsum = Normalize(imsum);
        
        % CLAHE
        CLAHE = adapthisteq(im);
        CLAHE = Normalize(CLAHE);
%         Wavelet 
        levels = [2,3];
        imw = iuwt_vessels(im,levels);
        imw = Normalize(imw);

%         B-Cosfire
            mask = ones(size(im));
            W = 15; step = 2;
            imbcosfire = im_seg(im,mask,W,step);
            imbcosfire = Normalize(imbcosfire);
 
    
        % Calculate the AUCs
            [~,~,~,AUCOrig] = perfcurve(gt(:),im(:),1);
            
            imda = round(imda,3);
            [~,~,~,AUC] = perfcurve(gt(:),imda(:),1);
  
            Vmax = round(Vmax,3);
            [~,~,~,AUCVes] = perfcurve(gt(:),Vmax(:),1);
      
            imf = round(imf,3);
            [~,~,~,AUCNe] = perfcurve(gt(:),imf(:),1);
        
            imP = round(imP,3);
            [~,~,~,AUCPctV] = perfcurve(gt(:),imP(:),1);
       
            imf2 = round(imf2,3);
            [~,~,~,AUCPctN] = perfcurve(gt(:),imf2(:),1);
       
            imsum = round(imsum,3);
            [~,~,~,AUCSsum] = perfcurve(gt(:),imsum(:),1);
       
            CLAHE = round(CLAHE,3);
            [~,~,~,AUCHE] = perfcurve(gt(:),CLAHE(:),1);
       
            RVR = round(RVR,3);
            [~,~,~,AUCVR]= perfcurve(gt(:),RVR(:),1);
            
            imw = round(imw,3);
            [~,~,~,AUCW] = perfcurve(gt(:),imw(:),1);
            
            imbcosfire = round(imbcosfire,3);
            [~,~,~,AUCB] = perfcurve(gt(:),imbcosfire(:),1);
      
        disp([psnr,AUCOrig,AUC,AUCVes,AUCNe,AUCPctV,AUCPctN,AUCSsum,AUCHE,AUCVR,AUCW,AUCB]);
        %Assign Data
        [~,idn] = ismember(noise,noisetypes);
        data(idn,psnr-3,1) = AUCOrig;
        data(idn,psnr-3,2) = AUC;
        data(idn,psnr-3,3) = AUCVes;
        data(idn,psnr-3,4) = AUCNe;
        data(idn,psnr-3,5) = AUCPctV;
        data(idn,psnr-3,6) = AUCPctN;
        data(idn,psnr-3,7) = AUCSsum;
        data(idn,psnr-3,8) = AUCHE;
        data(idn,psnr-3,9) = AUCVR;
        data(idn,psnr-3,10) = AUCW;
        data(idn,psnr-3,11) = AUCB;
        
    end
end

%%Save data
csvwrite('pepper.dat',[[4:25]',squeeze(data(1,:,:))]);
csvwrite('speckle.dat',[[4:25]',squeeze(data(2,:,:))]);
csvwrite('gaussian.dat',[[4:25]',squeeze(data(3,:,:))]);
% csvwrite('multifreq.dat',[[4:25]',squeeze(data(4,:,:))]);
return

%% Plot
for noise = 1:length(noisetypes)
    figure; hold on;%('Units','Normalized','Position',[0,0,1,1]); hold on;
    for method = 1:11
        plot(4:25,squeeze(data(noise,:,method)),'LineWidth',2); hold on;
    end
    set(gca,'XDir','reverse');
    xlabel('Peak Signal-to-Noise Ratio (dB)'); ylabel('Area Under ROC Curve (Normalised)');
    legend('Raw','Proposed','Vesselness','Neutriness','PCT Vesselness','PCT Neuriteness','Sum of TopHats','CLAHE','VolumeRatio','wavelet','bcosfire','Location','southoutside','Orientation','vertical');
end