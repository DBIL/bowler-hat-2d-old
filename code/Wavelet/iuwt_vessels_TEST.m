clc; clear; close all;
%%
im = imread('im0077.ppm');
gt = imread('im0077.ah.ppm');
im = im(:,:,2); % green plane image
im = imcomplement(im);
im = im2double(im);
gt = im2double(gt);
levels = [2,3];
imw = iuwt_vessels(im,levels);
%% demonstrate the results
figure, imshow(imw);colormap gray; axis off; axis equal; axis tight;
%% AUC results
imw = round(imw,3);
[AUC,~,~,xroc,yroc] = roc([imw(:),gt(:)]); auc  = AUC.AUC;
disp(AUC);
%% Save the result
% imwrite(imw,'im0077Wavelet.png');
