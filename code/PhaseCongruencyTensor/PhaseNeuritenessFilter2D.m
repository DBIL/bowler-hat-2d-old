function [imf,L1,L2,Lmin] = PhaseNeuritenessFilter2D(im, nscale, norient, minWaveLength, mult, sigmaOnf, k, cutOff, g, noiseMethod)
%%  NeuriteneesFilter - computing the neuritenees
%   
%   REFERENCE:
%       E. Meijering et al. Design and Validation of a Tool for Neurite 
%       Tracing and Analysis in Fluorescence Microscopy Images
%       Cytometry Part A, 58A, 167–176, 2004
%
%   INPUT:
%       im      - gray image
%       sigma   - sigma factor
%
%   OUTPUT:
%       imf     - filtered image
%
%   USAGE:
%
%   AUTHOR:
%       Boguslaw Obara, http://boguslawobara.net/
%
%   VERSION:
%       0.1 - 25/04/2010 First implementation

% Phase
[M,m,or,featType,PC,EO,T,pcSum] = phasecong3(im, nscale, norient, minWaveLength, mult, sigmaOnf, ...
                   k, cutOff, g, noiseMethod);
%% Normalize
imf = pcSum;
imf = Normalize(imf);
%% Vectors
norient = length(PC);
ang = ((1:norient)-1)*pi/norient;
qk = zeros(size(imf,1),size(imf,2),norient);
for o=1:norient
    qk(:,:,o) = PC{o};
end
%% Tensor
T = TensorForOrientedQuadratureFilters(qk,ang);
%% Eigen Matrix - values and vectors
[L1,L2,V1,V2,V3,V4] = EigenMatrix2x2M(T(:,:,1),T(:,:,2),T(:,:,3),T(:,:,4));
% Modified Hessian
alfa = -1;
l1=L1;
l2=L2;
L1 = l1 + alfa*l2;
L2 = l2 + alfa*l1;
% [L1,L2,V1,V2,V3,V4] = EigenSort2x2M(L1,L2,V1,V2,V3,V4);
% Sort L1s > L2s
index = abs(L1)<abs(L2);
L1s = L1; 
L2s = L2;
L1s(index) = L2(index);
L2s(index) = L1(index);
% %V1s = V1; 
% 
% V3s = V3;
% %V2s = V2; 
% V4s = V4;
% %V1s(index) = V3(index);
% V3s(index) = V1(index);
% %V2s(index) = V4(index);
% V4s(index) = V2(index);

%% Neuriteness
Lmin = min(L1(:));
imf = zeros(size(L1));
imf(L1<0) = L1(L1<0)./Lmin;
% Eigenvectors
%Vy = V4s; Vx = V3s;
Vy = V4; Vx = V3;
% name = strcat(name,'PCTN','.png');
% saveFunction(imf,'2D',name);
end