function [imf,Vx,Vy,L1,L2] = PhaseVesselnessFilter2D(beta,c,im,nscale,norient,minWaveLength,mult,sigmaOnf,k,cutOff, g, noiseMethod)
% beta = varargin{1};
% c = varargin{2};
% varargin(1:2) = [];
%% Phase
[~,~,~,~,PC,~,~,pcSum] = phasecong3(im, nscale, norient, minWaveLength, mult, sigmaOnf, ...
                   k, cutOff, g, noiseMethod);
%% Normalize
imf = pcSum;
imf = Normalize(imf);
%% Vectors
norient = length(PC);
ang = ((1:norient)-1)*pi/norient;
qk = zeros(size(imf,1),size(imf,2),norient);
for o=1:norient
    qk(:,:,o) = PC{o};
end
%% Tensor
T = TensorForOrientedQuadratureFilters(qk,ang);
%% Eigen Matrix - values and vectors
[L1,L2,V1,V2,V3,V4] = EigenMatrix2x2M(T(:,:,1),T(:,:,2),T(:,:,3),T(:,:,4));
%%
index = abs(L1)>abs(L2); 
L1s = L1; 
L2s = L2;
L1s(index) = L2(index);
L2s(index) = L1(index);
L1 = L1s;
L2 = L2s;
L2(L2==0) = eps;
%% Compute Filtered Image    
Rbeta = L1./L2;
S = sqrt(L1.^2 + L2.^2);     
imf = exp(-(Rbeta.^2)/(2*beta^2)).*(ones(size(imf))-exp(-(S.^2)/(2*c^2)));    
%% Vectors
%V1s = V1; 
V3s = V3;
%V2s = V2; 
V4s = V4;
%V1s(index) = V3(index);
V3s(index) = V1(index);
%V2s(index) = V4(index);
V4s(index) = V2(index);
%Vx = V3; Vy = V4;
Vy = V4s; Vx = V3s;
%% Normalize
% imf = (varargin{1} + imf);
imf = double(imf); imf = (imf - min(imf(:))) / (max(imf(:)) - min(imf(:)));
% name = strcat(name,'PCTV','.png');
% saveFunction(imf,'2D',name);
end