clc, clear all, close all;
%% read image
%  im = imread ('1.png');
%  im = imread('Series005_0.tif');
% im = imread('collagen-mip.tif');
%  im = imread('fungus.tif');
% im = rgb2gray(im);
im = imread ('leaf2.tif');    % retinal imaging
% im = im(:,:,2); % green plane image
im = imcomplement(im);
% s = 5; h = fspecial('gaussian',s); 
% im = imfilter(double(im),h);
im = Normalize(im);
% im =imcomplement(im);
%%
nscale              = 4; 
norient             = 5;
minWaveLength       = 3;
mult                = 2.1; 
sigmaOnf            = 0.65;
k                   = 8;
cutOff          = 0.5;
g               = 5;               
noiseMethod     = -1;
%                        
[imf,L1,L2,Lmin] = PhaseNeuritenessFilter2D(im, nscale, norient, minWaveLength, mult, sigmaOnf, k, cutOff, g, noiseMethod);
%%
imf= Normalize (imf);
figure, imagesc(imf);colormap gray; axis equal; axis tight; axis off;
% figure, imshow(L1);  title ('L1');
% figure, imshow(L2); title ('L2');