clc, clear all, close all;
%%
%  im = imread ('10007.tif');
%  im = imread('Series005_0.tif');
% im = imread('collagen-mip.tif');
%  im = imread('fungus.tif');
% im = rgb2gray(im);
% s = 5; h = fspecial('gaussian',s); 
% im = imfilter(double(im),h);

% im =imcomplement(im);
im = imread ('im0001.tif');    % retinal imaging
im = im(:,:,2); % green plane image
% im = imcomplement(im);
im = Normalize(im);
%% Image Create
% gt = zeros(100,100);
% gt(20,25) = 1;
% gtB = bwdist(gt)<4;
% gt(60,15:60) = 1;
% gtA = bwdist(gt)<4;
% gt(90,15:60) = 1;
% gtC = bwdist(gt)<2;
% gt = max(gt,gtA);
% gt = max(gt,gtB);
% gt = max(gt,gtC);
% gt = logical(gt);
% clear gtA gtB;
% im = double(gt);
% gt = logical(gt);
% im = imgaussfilt(im,2);
%% Parameters
nscale              = 4; 
norient             = 5;
minWaveLength       = 2.6;
mult                = 2.1; 
sigmaOnf            = 0.35;
k                   = 8;
cutOff          = 0.5;
g               = 10;               
noiseMethod     = -1;
beta                = 10; 
c                   = 15;
%% Calculate Phase Vesselness
[imP,Vx,Vy] = PhaseVesselnessFilter2D(beta,c,im,nscale,norient,minWaveLength,mult,sigmaOnf,k,cutOff, g, noiseMethod);
%%
figure, imshow(imP);
% figure, imshow(Vx);
% figure, imshow(Vy);