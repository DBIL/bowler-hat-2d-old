%% clear
clc; clear all; close all;

%% path
addpath('./lib')

%% load image
im = imread('40_training.tif');
im = rgb2gray(im);

%% normalize
im = double(im); im = (im - min(im(:))) / (max(im(:)) - min(im(:)));
im = imcomplement(im);

%% opening by reconstruction
s = 10; % line size
no = 12; % number of orientation
o = 0:180/no:180-180/no;

immax = zeros(size(im));
for i=1:length(o)
  se = strel('line',s,o(i));
  imo = imopen(im,se);
  immax = max(immax,imo);
end
imop = imreconstruct(im,immax);
figure; imagesc(imop); colormap gray; axis off; axis equal; axis tight;

%% sum
imsum = zeros(size(im));
for i=1:length(o)
  se = strel('line',s,o(i));
  imo = imopen(im,se);
  imsum = imsum + (imop - imo);
end
figure; imagesc(imsum); colormap gray; axis off; axis equal; axis tight;
%% laplacian
sigma = 3;
h = fspecial('log',3*[sigma sigma],sigma);
imlap = imfilter(imsum,h,'same');
imlap = -imlap; % fix
figure; imagesc(imlap); colormap gray; axis off; axis equal; axis tight;

%% alternating filter 1
immax = -inf*ones(size(im));
for i=1:length(o)
  se = strel('line',s,o(i));
  imo = imopen(imlap,se);
  immax = max(immax,imo);
end
im1 = imreconstruct(immax,imlap);
%im1 = imreconstruct(imlap,immax);
figure; imagesc(im1); colormap gray; axis off; axis equal; axis tight;

%% alternating filter 2
immin = inf*ones(size(im));
for i=1:length(o)
  se = strel('line',s,o(i));
  imc = imclose(im1,se);
  immin = min(immin,imc);
end
im2 = imreconstruct(immin,im1);
%im2 = imreconstruct(im1,immin);
figure; imagesc(im2); colormap gray; axis off; axis equal; axis tight;

%% alternating filter res
s = 2*s; % scaling factor e
immax = -inf*ones(size(im));
for i=1:length(o)
  se = strel('line',s,o(i));
  imo = imopen(im2,se);
  immax = max(immax,imo);
end
imres = immax>=1;
figure; imagesc(immax); colormap gray; axis off; axis equal; axis tight;
figure; imagesc(immax>0); colormap gray; axis off; axis equal; axis tight;

%% plot
% figure; imagesc(im); colormap gray; 
% set(gca,'ytick',[]); set(gca,'xtick',[]); axis image; axis tight;
% figure; imagesc(imres); colormap gray; 
% set(gca,'ytick',[]); set(gca,'xtick',[]); axis image; axis tight;