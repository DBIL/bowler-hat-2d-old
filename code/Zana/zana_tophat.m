function [imsum,imlap,immax] =zana_tophat(im,s,no)

%% opening by reconstruction
o = 0:180/no:180-180/no;

immax = zeros(size(im));
for i=1:length(o)
  se = strel('line',s,o(i));
  imo = imopen(im,se);
  immax = max(immax,imo);
end
imop = imreconstruct(im,immax);
%% sum
imsum = zeros(size(im));
for i=1:length(o)
  se = strel('line',s,o(i));
  imo = imopen(im,se);
  imsum = imsum + (imop - imo);
end
%% laplacian
sigma = 3;
h = fspecial('log',3*[sigma sigma],sigma);
imlap = imfilter(imsum,h,'same');
imlap = -imlap; % fix
%% alternating filter 1
immax = -inf*ones(size(im));
for i=1:length(o)
  se = strel('line',s,o(i));
  imo = imopen(imlap,se);
  immax = max(immax,imo);
end
im1 = imreconstruct(immax,imlap);

%% alternating filter 2
immin = inf*ones(size(im));
for i=1:length(o)
  se = strel('line',s,o(i));
  imc = imclose(im1,se);
  immin = min(immin,imc);
end
im2 = imreconstruct(immin,im1);

%% alternating filter res
s = 2*s; % scaling factor e
immax = -inf*ones(size(im));
for i=1:length(o)
  se = strel('line',s,o(i));
  imo = imopen(im2,se);
  immax = max(immax,imo);
end
end


