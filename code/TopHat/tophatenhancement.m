function S_sum = tophatenhancement(S_0)

gamma_L = zeros(size(S_0));
for i=0:15:179
    se = strel('line',15,i);
    gamma_Li = imopen(S_0,se);
    gamma_L = max(gamma_L,gamma_Li);
end

S_op = imreconstruct(S_0,gamma_L);

S_sum = zeros(size(S_0));
for i=0:15:179
    se = strel('line',15,i);
    gamma_Li = imopen(S_0,se);
    S_sum = S_sum + (S_op - gamma_Li);
end
