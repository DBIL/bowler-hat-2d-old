function im = GaussianConvND(im,s,o)
ds = ndims(im);  % number of dimensions
% if numel(o)~=ds
%     error([mfilename ': o == ds !']);
% end
%% Convolve in each dimension
for d=1:ds
    % create kernel in appropriate direction
    g = GaussianFilterN1D(s,o(d));
    % shift the dimension of the kernel
    g = shiftdim(g,-(d-1));
    % convolve
    im = imfilter(im,g,'symmetric','same' );
end
end