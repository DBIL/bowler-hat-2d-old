%% This function generates noisy 2D/3D images at a desired PSNR.
%  It may not get the exact PSNR, therefore the found PSNR is also returned.
%
%  Example usage, generate images corrupted at PSNR 10:
%      figure, imshow(noiseAtPSNR(I, 10, 'gaussian', randi(intmax)));
%      figure, imshow(noiseAtPSNR(I, 10, 'multifreq', randi(intmax)));
%
function [O, peaksnr, signr] = noiseAtPSNR(I, target, type, seed)

    if nargin <= 3; seed = randi(intmax); end;
    if nargin <= 2; type = 'guassian'; end;

    I = double(I);
    O = double(I);

    switch type
        case 'pepper'
%             noiseFunc = @(x)imnoise(I, 'salt & pepper', max(min(x/100,1.0),0));
            noiseFunc = @(x)pepperNoise(I, max(min(x/100,1.0),0));
        case 'speckle'
            noiseFunc = @(x)speckleNoise(I, max(min(x/100,1.0),0));
        case 'multifreq'
            noiseFunc = @(x)multifreqNoise(I, max(x,eps));
        otherwise
            noiseFunc = @(x)awgn(I, x);
    end

    function fval = objectiveFunc(val)
        rng(seed);
        O = noiseFunc(val);
        fval = abs(target-psnr(I,O));
    end

    options = gaoptimset('PopulationSize',50, 'InitialPopulation', linspace(0,100,50)','Display','off');
    [x,~] = ga(@objectiveFunc, 1,[],[],[],[], 0, 100, [], options);

    objectiveFunc(x);
    [peaksnr, signr] = psnr(I,O);

end
