% 2D speckle noise within wider parameter range than imnoise
% inspired by https://uk.mathworks.com/matlabcentral/fileexchange/1360-noise/content/noise.m
function O = speckleNoise(I, val)
    
    [m,n] = size(I);
    O = I;
    
    scale = double(max(I(:)));
    
    m_incid = rand(m,n);
    m_incid = find(m_incid <= val);
    
    O(m_incid) = scale*randn(size(m_incid));
    
end