% 2D pepper noise within wider parameter range than imnoise
% inspired by https://uk.mathworks.com/matlabcentral/fileexchange/1360-noise/content/noise.m
function O = pepperNoise(I, val)

    [m,n] = size(I);
    O = I;
    
    m_incid = rand(m,n);
    m_incid = find(m_incid <= val);
    
    n(m_incid) = sign(randn(size(m_incid)));
    
    umax = max(I(:));
    umin = min(I(:));
    
    O(n==-1) = umax;%salt
    O(n==1) = umin;%pepper
end