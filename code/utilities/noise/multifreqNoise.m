% 2D/3D Multi-frequency noise function
function O = multifreqNoise(I, val, freqSpread)

    if (nargin==2)
        freqSpread = 1.7;
    end
    
    [yn,xn,zn] = size(I);
    
    yn = max(yn,xn);
    xn = max(xn,yn);
    
    [xm,ym,zm] = meshgrid(1:xn,1:yn,1:zn);
    d = sqrt((xm-xn/2).^2 + (ym-yn/2).^2 + (zm-zn/2).^2);
    Z = d.^(-freqSpread) .* exp(rand(yn,xn,zn)*j*2*pi);
    Z = max(min(real(Z),1),-1);
    Z = ifftn(ifftshift(Z));
    
    O = imcrop(real(Z), [1,1,size(I,2)-1, size(I,1)-1]);
    O = (1-val).*I + val.*mat2gray(abs(O));
end

