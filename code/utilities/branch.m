%% Synthetic image with known branch angle
function gt = branch(angle)
% angle should be in degrees

%% Set-Up
angle = mod(angle,180);
vesselRadius = 4;%gives thickness of 9
gt = zeros(100,100);

%% Incoming Vessel
gt(50,1:50) = 1;

%% Outgoing Vessels
dx = linspace(0,50,5100);
theta = angle/2;

% Upper
dy = dx*tand(theta);
x = dx+50;
y = dy+50; x(y>100)=[]; y(y>100)=[];
index = sub2ind(size(gt),round(y),round(x));%indices of the line
gt(index) = 1;

% Lower
dy = dx*tand(-theta);
x = dx+50;
y = dy+50; x(y<0.5)=[]; y(y<0.5)=[];
index = sub2ind(size(gt),round(y),round(x));%indices of the line
gt(index) = 1;

%% Set Thickness
gt = bwdist(gt)<vesselRadius;

end
