function AUC = ROCmanuel(im,gt)
%% Threshold
TP = zeros(10,1); FP = zeros(10,1);
TN = zeros(10,1); FN = zeros(10,1);
for th=1:10
    %% Segment enhancement
    seg = logical(im>=(th/10));
%     figure, imshow(imfuse(seg,gt))
    %% Calculate TP, FP, TN and FN for binary result
%     TFim = zeros(size(im));
%     TFim(seg(:)&gt(:)) = 1;
%     TFim(seg(:)&~gt(:)) = 2;
%     TFim(~seg(:)&~gt(:)) = 3;
%     TFim(~seg(:)&gt(:)) = 4;
%     figure, imagesc(TFim); colormap jet; colorbar; caxis([1,4])
    TP(th) = sum( seg(:) &  gt(:));
    FP(th) = sum( seg(:) & ~gt(:));
    TN(th) = sum(~seg(:) & ~gt(:));
    FN(th) = sum(~seg(:) &  gt(:));
end
%% Calculate ROC area
% A ROC space is defined by FPR and TPR as x and y axes respectively
% Since TPR is equivalent to sensitivity and FPR is equal to 1 ? specificity,
triv = (TP+FN)==0;
TPR = TP./(TP+FN);
TPR(triv)=0;
triv = (TN+FP)==0;
FPR = FP./(FP+TN);
FPR(triv)=0;

%% plot roc curve
[FPR,ids] = sort(FPR);
TPR = TPR(ids);
hold on; plot(FPR,TPR); xlabel('FPR') ;ylabel('TPR'); xlim([0,1]); ylim([0,1])
AUC =1-trapz(FPR,TPR);% NB will give zero if no variation in FPR
end