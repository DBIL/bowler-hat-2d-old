function [Sensitivity,Specificity,Precision,FPR,FNR,ACC]=stats(TP,TN,FN,FP)
Sensitivity = sum(TP(:)) / (sum(TP(:)) + sum(FN(:)));
Specificity = sum(TN(:)) / (sum(TN(:)) + sum(FP(:)));
Precision = sum(TP(:)) / (sum(TP(:)) + sum(FP(:)));
FPR = sum(FP(:)) / (sum(FP(:)) + sum(TN(:)));
FNR = sum(FN(:)) / (sum(FN(:)) + sum(TP(:)));
ACC = ( sum(TP(:)) + sum(TN(:)) ) / (sum(TP(:)) + sum(TN(:)) + sum(FP(:)) + sum(FN(:)));
end