function saveFunction(im,f,name)
%%  
    if(f == '3D')
    %save 3d
      for i=1:size(im,3)
         imwrite(im(:,:,i),name,'Compression','none','WriteMode','append');
      end
    else if(f == '2D')
    % save 2d
    im = max(im,[],3);
    imwrite(im,name);
    end
    end
%%
% file='1.tif';
% im=ReadImage3D(file);
%%
% im=Normalize(im);
% for i=1:size(im,3);
%          imwrite(im(:,:,i),'1n.png','Compression','none','WriteMode','append');
% end