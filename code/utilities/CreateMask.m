imFiles = dir('C:\Users\Cigdem\GIT\bowler-hat-2d\image\STARE DATABASE\stare-images\*.ppm');
for i = 1 : length(imFiles)
  filename = imFiles(i).name;
  im = imread(filename);
  im = im(:,:,2);
  mask = (im > 45) & (im < 255);
  mask = bwareaopen(mask,20);
  mask = imopen(mask,strel('disk',5));
  mask = imclose(mask,strel('square',10));
  imwrite(mask,strcat('stareMask\',filename));
  figure,imshow(mask);
end
