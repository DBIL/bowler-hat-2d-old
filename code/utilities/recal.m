function [TP,TN,FP,FN] = recal(gt,imth)
[m,n]= size(gt);
TN = zeros(size(gt,1),size(gt,2));%gt 0 seg 0
FP = zeros(size(gt,1),size(gt,2));%gt 0 seg 1
TP = zeros(size(gt,1),size(gt,2));%gt 1 seg 1
FN = zeros(size(gt,1),size(gt,2));%gt 1 seg 0
for i=1:m
    for j = 1:n
        if(gt(i,j)==0 && imth(i,j)==0)
                TN(i,j)=1;
                FP(i,j)=0;
                TP(i,j)=0;
                FN(i,j)=0;
         elseif(gt(i,j)==0 && imth(i,j)==1)
                FP(i,j)=1;  
                TN(i,j)=0;
                TP(i,j)=0;
                FN(i,j)=0;
        elseif(gt(i,j)==1 && imth(i,j)==0)
                FP(i,j)=0;  
                TN(i,j)=0;
                TP(i,j)=0;
                FN(i,j)=1;   
        elseif(gt(i,j)==1 && imth(i,j)==1)
                FP(i,j)=0;  
                TN(i,j)=0;
                TP(i,j)=1;
                FN(i,j)=0;
        end
    end
end
end