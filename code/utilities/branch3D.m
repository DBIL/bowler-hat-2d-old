%% Synthetic image with known branch angle
function gt = branch3D(angle)
% angle should be in degrees

%% Set-Up
angle = mod(angle,180);
vesselRadius = 4;%gives thickness of 9
gt = zeros(100,100,100);

%% Incoming Vessel
gt(50,1:50,50) = 1;

%% Outgoing Vessels
dx = linspace(0,50,5100);
theta = angle/2;
phi = angle/3;

% Upper
dy = dx*tand(theta);
x = dx+50;
y = dy+50; 
z = dx+50;x(y>100)=[]; z(y>100)=[]; y(y>100)=[];
index = sub2ind(size(gt),round(y),round(x),round(z));%indices of the line
gt(index) = 1;

% Lower
dy = dx*tand(-theta);
x = dx+50;
y = dy+50; 
z = dx+50; x(y<0.5)=[]; z(y<0.5)=[]; y(y<0.5)=[]; 
index = sub2ind(size(gt),round(y),round(x),round(z));%indices of the line
gt(index) = 1;

% Another
dy = dx*tand(phi);
x = dx+50;
y = dy+50; 
z = dx+50;x(y>100)=[]; z(y>100)=[]; y(y>100)=[];
index = sub2ind(size(gt),round(y),round(x),round(z));%indices of the line
gt(index) = 1;
% Another
dy = dx*tand(-phi);
x = dx+50;
y = dy+50; 
z = dx+50; x(y<0.5)=[]; z(y<0.5)=[]; y(y<0.5)=[]; 
index = sub2ind(size(gt),round(y),round(x),round(z));%indices of the line
gt(index) = 1;

%% Set Thickness
gt = bwdist(gt)<vesselRadius;

end
