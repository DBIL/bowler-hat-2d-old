%% Syntethic image
function [gt,gtV] = blobves(sep)
% note: gtV is purely vessel
vesselRadius = 4;
blobRadius = 10;

%% create blob
gtB = zeros(100,100);
gtB(50,round(50-(sep*max(vesselRadius,blobRadius)))) = 1;
gtB = bwdist(gtB)<blobRadius;

%% create vessel
gtV = zeros(100,100);
gtV(10:90,50) = 1;
gtV = bwdist(gtV)<vesselRadius;

%% combine
gt = max(gtB,gtV);
end
