function [Hxx,Hyy,Hzz,Hxy,Hxz,Hyz] = HessianMatrix3D(im,s)
%% Gaussian 2nd derivatives
[Gxx,Gyy,Gzz,Gxy,Gxz,Gyz] = Gaussian2ndDerivatives3D(s);
%% Hessian Matrix
Hxx = imfilter(im,Gxx,'conv','same','replicate');
Hyy = imfilter(im,Gyy,'conv','same','replicate');
Hzz = imfilter(im,Gzz,'conv','same','replicate');
Hxy = imfilter(im,Gxy,'conv','same','replicate');
Hxz = imfilter(im,Gxz,'conv','same','replicate');
Hyz = imfilter(im,Gyz,'conv','same','replicate');
%% End
end