%% Syntethic image
function [im,gt] = Synthetic3D(type,noise)
%% create image
gt = zeros(100,100,100);
switch type
    case 'blob'
        gt(50,50,50) = 1;
        gt = bwdist(gt)<10;
        gt(50,25:75,50) = 1;
        gt= bwdist(gt)<4;
    case 'line'
        gt(50,25:75,50) = 1;
        gt= bwdist(gt)<4;
    case {'curve','branch'}
        gt = branch3D(90);
    case 'cross'
        gt(50,1:99,50) = 1;
        gt(1:99,50,50) = 1;
        gt = bwdist(gt)<4;
    case 'plane'
        gt(40:70,40:70,50) = 1;
        gt = bwdist(gt)<4;
    case 'blobVes'
        gt = blobves3D(1);
        gt(10:90,50,50) = 1;
        gt = bwdist(gt)<2;
    case 'bend'
        gt = mat2gray(imread('bend.png'));
        gt= bwdist(gt)<5;
end
gt = logical(gt);
im = double(gt);
%% noise to image
switch noise
    case'salt'
    im = imnoise(im,'salt & pepper');
    im = imgaussfilt3(im,2);
    case'gaussian'
    im = imnoise(im,'gaussian');
    im = imgaussfilt3(im,2);
    case 'no'
    im = imgaussfilt3(im,2);
    otherwise
end

% figure, imagesc(im); colormap jet; axis off; axis equal; axis tight;
