function imrgb = NetworkRGB3D(im,imrm,imgm,imbm)
%% RGB
imrgb = zeros(size(im,1),size(im,2),3);
imr = im; img = im; imb = im;
imr(imrm) = 1; img(imrm) = 0; imb(imrm) = 0;
imr(imgm) = 0; img(imgm) = 1; imb(imgm) = 0;
imr(imbm) = 1; img(imbm) = 1; imb(imbm) = 1;
imrgb(:,:,1) = imr; imrgb(:,:,2) = img; imrgb(:,:,3) = imb;
end