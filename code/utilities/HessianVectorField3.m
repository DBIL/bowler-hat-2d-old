function [L1,L2,L3,V1,V2,V3,V4,V5,V6,V7,V8,V9] = HessianVectorField3(im,s,o)
%%  HessianVectorField - 
%   
%   REFERENCE:
%
%   INPUT:
%       im      - gray image
%       s       - sigma
%
%   OUTPUT:
%       L1,L2   - eigenvalues
%       V1,V2   - eigenvectors (x,y)
%       V3,V4   - eigenvectors (x,y)
%
%   USAGE:
%
%   AUTHOR:
%       Boguslaw Obara, http://boguslawobara.net/
%
%   VERSION:
%       0.1 - 08/03/2010 First implementation

%% Normalize image
im = double(im); im = (im - min(im(:))) / (max(im(:)) - min(im(:))); 
%% Second Derivatives - Hessian Matrix
[Hxx,Hyy,Hzz,Hxy,Hxz,Hyz] = HessianMatrix3D(im,s);
%% Normalized Derivative - Scale
Hxx = power(s,2)*Hxx;
Hxy = power(s,2)*Hxy;
Hxz = power(s,2)*Hxz;
Hyy = power(s,2)*Hyy;
Hyz = power(s,2)*Hyz;
Hzz = power(s,2)*Hzz;
%% Eigen Matrix - values and vectors
[L1,L2,L3,V1,V2,V3,V4,V5,V6,V7,V8,V9] = EigenMatrix3x3M(Hxx,Hxy,Hxz,Hxy,Hyy,Hyz,Hxz,Hyz,Hzz);
end