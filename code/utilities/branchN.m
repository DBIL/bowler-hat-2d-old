%% Synthetic image with known number of branches
function gt = branchN(num)
%% Set-Up
angle = 360/num;
vesselRadius = 4;%gives thickness of 9
gt = zeros(100,100);
theta = randi(round(angle));

%% Vessels
for i=1:num
    dx = linspace(0,50,5100);
    theta = theta+angle;

    dy = dx*tand(theta);
    x = dx+50;
    y = dy+50;
    x(y>100)=[]; y(y>100)=[];
    x(y<0.5)=[]; y(y<0.5)=[];
    index = sub2ind(size(gt),round(y),round(x));%indices of the line
    gt(index) = 1;
end

%% Set Thickness
gt = bwdist(gt)<vesselRadius;

end
