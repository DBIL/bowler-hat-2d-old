%% Syntethic image
function [im,gt] = Synthetic(type,noise)
%% create image
gt = zeros(100,100);
switch type
    case 'blob'
        gt(50,50) = 1;
        gt = bwdist(gt)<10;
        gt(50,25:75) = 1;
        gt= bwdist(gt)<4;
    case 'line'
        gt(50,25:75) = 1;
        gt= bwdist(gt)<4;
    case 'line-blob'
        gt(25,50) = 1;
        gt(50,25:75) = 1;
        gt= bwdist(gt)<4;
    case {'curve','branch'}
        gt = branch(45);
    case 'cross'
        gt(50,1:99) = 1;
        gt(1:99,50) = 1;
        gt = bwdist(gt)<4;
    case 'plane'
        gt(40:70,40:70) = 1;
        gt = bwdist(gt)<3;
    case 'blobVes'
        [im,gt] = blobves(1);
    case 'bend'
        gt = mat2gray(imread('bend0001.png'));
        gt= bwdist(gt)<5;
end
gt = logical(gt);
if strcmp(type,'blobVes')
    im = double(im);
else
    im = double(gt);
end
%% noise to image
switch noise
    case'salt'
    im = imnoise(im,'salt & pepper');
    im = imgaussfilt(im,2);
    case'gaussian'
    im = imnoise(im,'gaussian');
    im = imgaussfilt(im,2);
    case 'no'
    im = imgaussfilt(im,2);
    otherwise
end

% figure, imagesc(im); colormap jet; axis off; axis equal; axis tight;
