%% Clear ALL
clc; clear all; close all;
%% Load Image
% file='1.png';
% im=imread(file);
% [pathstr,name,ext] = fileparts(file);
% name = strcat(name,'Vesselness');
% im = rgb2gray(im);

% im = Synthetic('cross','no'); 
im = imcomplement(im);
%% Calculate Vesselness
sigma = 1:0.5:3; gamma = 0.5; beta = 0.5; c = 15; wb = true;
%%
[V,Vmax] = Vesselness2D(im,sigma,gamma,beta,c,wb);
%% Plot
figure;
subplot(1,2,1), imagesc(im);
colormap gray; axis off; axis equal; axis tight;
subplot(1,2,2), imagesc(Vmax);
colormap gray; axis off; axis equal; axis tight;
%   
%% 

% figure, imagesc(Vmax); colormap gray; axis off; axis equal; axis tight;
% name = strcat(name, '.png'); 
% saveFunction(Vmax,'2D',name);