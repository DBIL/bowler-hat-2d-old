clc; clear; close all;
%%
im = imread('im0077.ppm');
gt = imread('im0077.ah.ppm');
mask = imread('maskim0077.ppm');


W = 15;
step = 2;
segmentedimg = im_seg(im,mask,W,step);
%%
return
[X,Y,T,AUC2] = perfcurve(gt(:),segmentedimg(:),1);
 segmentedimg = round(segmentedimg,3);
[AUC,~,~,xroc,yroc] = roc([segmentedimg(:),gt(:)]); auc  = AUC.AUC;
AUC2 = ROC(segmentedimg,gt);
disp([auc,AUC2]);
return
%% Save the result
im =rgb2gray(im);
im = im2double(im);
[Binput,inputImage] = bwboundaries(im);
[Benahnced,enhanced] = bwboundaries(segmentedimg);
[Bth,thresholded] = bwboundaries(segmentedimgBin);
figure,imshow(im+inputImage,[]);colormap gray; axis off; axis equal; axis tight;
hold on
for k = 1:length(Binput)
   boundary = Binput{k};
   plot(boundary(:,2), boundary(:,1), 'r', 'LineWidth', 2)
end
figure,
subplot(132),imshow(segmentedimg+enhanced,[]);colormap gray; axis off; axis equal; axis tight;
hold on
for k = 1:length(Benahnced)
   boundary = Benahnced{k};
   plot(boundary(:,2), boundary(:,1), 'r', 'LineWidth', 2)
end
figure,imshow(segmentedimgBin+thresholded,[]);colormap gray; axis off; axis equal; axis tight;
for k = 1:length(Bth)
   boundary = Bth{k};
   plot(boundary(:,2), boundary(:,1), 'r', 'LineWidth', 2)
end