% !TEX root = ../main.tex
\section{Introduction}
\label{sec:intro}
Retinal vessel segmentation and measurement of blood vessels are used for diagnosis, screening and evaluation for a range of diseases including diabetes and arteriosclerosis~\cite{datta2015new}. Manual segmentation of the vasculature is both time consuming and requires expertise and patience~\cite{MC2006}. However, automated segmentation methods often struggle with raw retinal imaging and require an image enhancement preprocessing step~\cite{gonzalez2014segmentation}.

The vasculature in retinal images appear as elongated, branching structures with a wide variety of vessel widths and tortuosities and usually a wide range of intensities and contrast. Such images also include various non-vasculature objects, such as the fovea. These challenges make retinal imaging an ideal case study for the development of new vessel enhancement measurements.

Extraction of retinal vessels and their network is often a key step in the quantified or automated analysis of such images for automated diagnoses. However, retinal image may suffer from poor quality due to many reasons, including, but not limited to, noisy image capture, sample/patient variability and low contrast scenarios. As such, standard segmentation or network extraction methods may not work on the raw image and some form of vessel enhancement is required (see~\cite{Fraz2012} for a recent review).

There are many approaches to vessel enhancement, such as Hessian matrix approaches~\cite{frangi1998multiscale,yang2014fast}, phase congruency tensor methods~\cite{obara2012contrast}, contrast limited adaptive histogram equalisation~\cite{Pisano1998} and many more. These methods all show increased segmentation performance compared to raw images, vasculature extraction of fundus images. However, many existing methods have practical issues, such as the loss of signals at junctions (\cref{fig:prob:junc}), false introduction of vessel-like structures (\cref{fig:prob:false}), complex parameter spaces and more.

\begin{figure}[h]
	\centering
	\begin{subfigure}[b]{0.32\linewidth}
		\centering
		\includegraphics[width=.95\linewidth]{crossNeurite}
		\caption{Junction artifacts}\label{fig:prob:junc}
	\end{subfigure}~
	\begin{subfigure}[b]{0.32\linewidth}
		\centering
		\includegraphics[width=.95\linewidth]{falseVessels}
		\caption{False vessels}\label{fig:prob:false}
	\end{subfigure}
	\caption{Some of the challenges faced when using many current and state-of-the-art vessel enhancement methods.}\label{fig:prob}
\end{figure}
\vspace{-20px}

\subsection{Contribution and Organisation}
In this paper, we introduce a new retinal vessel enhancement approach based on mathematical morphology and the elongated shape of vessel structures. The proposed method, the bowler-hat transform, shows visually and quantitatively improved results on a range of test, synthetic images and also on three public databases of fundus images: the DRIVE, STARE and HRF collections.

The rest of this paper is organised as follows. In \cref{sec:related}, we introduce key papers and approaches from the vessel enhancement literature. \Cref{sec:methods} introduces and explains our approach. \Cref{sec:results} presents validation experiments and results on synthetic and real data. Finally, we conclude this paper and discuss the future of this approach.
\vspace{-10px}

\section{Related Work}
\label{sec:related}
\subsection{Hessian-based Enhancement Methods}
\label{subsec:hessian}

Hessian matrix-based methods have been very popular in image enhancement methods. All these methods stem from the early work by Frangi and colleagues~\cite{frangi1998multiscale} who used the eigenvalues of Hessian matrices at various scales to enhance curvilinear/tubular objects in 2/3D respectively~\cite{frangi1998multiscale,krissian2000model}. Here we describe and compare three Hessian matrix-based enhancement methods: 1) the original `vesselness' measurement as introduced in~\cite{frangi1998multiscale}, the `neuriteness' measurement described in~\cite{meijering2004design} and a recently proposed measurement based on the regularised volume ratio in~\cite{JPLetal2016}.

\subsubsection{Vesselness}
\label{subsec:ves}
The vesselness measurement describes the images in terms of the eigenvalues of the Hessian matrix. For vessel-like structures, \ie~elongated, smooth structures, we see the following eigenvalue relationship:
\begin{equation}
	\lambda_2 \gg \lambda_1,
\end{equation}
where the eigenvalues are defined by the elements of the Hessian matrix, $H_{ij} \text{ and } (i,j)\in [1,2]$, such that
\begin{equation}
	\lambda_{ 1,2 }=\frac { 1 }{ 2 } \left( { H }_{ 11 }+{ H }_{ 22 } \pm\sqrt { {\left( { H }_{ 11 }-{ H }_{ 22 } \right)} ^{ 2 }+4{ H }_{ 12 }^{ 2 } }  \right).
\end{equation}
In general, if the magnitude of both eigenvalues is small, then the pixel is likely to be background; if $\lambda_1$ is small and $\lambda_2$ high then the feature is elongated, \eg~vessel-like; and if both are high then the feature is blob-like. This however leads to a failure at the intersection of vessels as both eigenvalues to towards zero and thus that feature can be lost\cite{krissian2003multiscale}.
In the original paper by Frangi and colleagues~\cite{frangi1998multiscale}, the vesselness measurement is then defined based on the ratio of eigenvalues.
In the original paper by Frangi and colleagues~\cite{frangi1998multiscale}, the vesselness measurement is then defined based on the ratio of eigenvalues as;
\begin{equation}
	\mathcal{V}_o =\begin{cases}
	0 & \text{if}~\lambda_2>0, \\
	\left(e^{-\dfrac{\mathcal{R}_\mathcal{B}^2}{2\beta^2}}\right)\left( 1-e^
	{ -\dfrac{\mathcal{S}^2}{2c^2}} \right) & ,
	\end{cases}
	\label{eq:frangi}
\end{equation}
where
\begin{equation}
	\mathcal{R}_\mathcal{B}=\lambda _{1}/\lambda_{2}, \quad
	\mathcal{S} = \sqrt{\lambda_1^2 + \lambda_2^2},
\end{equation}
where generally $\beta$ is fixed to 0.5 and $c$ is equal to half of the maximum Frobenius norm.

\subsubsection{Neuriteness}
\label{subsec:neurite}
An alternative to the vesselness measure was developing by Meijering and colleagues in~\cite{meijering2004design} to enhance low contrast and highly inhomogeneous images of neurites. This neuriteness measurement consists of a detection stage~\cite{levitan2015neuron}, which infers a putative neurite in every pixel of the image that has a non-zero value; and a tracing stage, which determines the pixels that are most likely to reflect the centre-lines of true neurites~\cite{capowski1989computer}.
In~\cite{meijering2004design}, Meijering and colleagues use a modified Hessian matrix, $H'$, based on second-order derivatives, with a tunable parameter, $\alpha$, to calculate a measure of neuriteness such that,
\begin{equation}
	N=
	\begin{cases}
		\dfrac{\lambda_{\max}}{\lambda_{\min}} \quad & $if$\quad \lambda_{\max} <0,   \\
		0\quad                                       & $if$\quad \lambda_{\max}\ge 0,
	\end{cases}
	\label{eq:neurite}
\end{equation}
where
\begin{align}
		  & \lambda_{\max} =\max(|{\lambda_{1}}'|,|{\lambda_{2}}'|), \\
		  & \lambda_{\min} =  \min(\lambda_{\max}),                  \\
		  & \lambda_1^{'} =\lambda _ 1 +\alpha \lambda _{ 2 },       \\
		  & \lambda_2^{'}=\lambda _{ 2 } +\alpha \lambda _{ 1 },
	\end{align}
where  $ { \lambda  }_{ i }^{ ' }$ are the normalized eigenvalues of $H'$; $\lambda_{\max}$ is the largest eigenvalue at each pixel; and $\lambda_{\min}$ the smallest value of all $\lambda_{\max}$~\cite{meijering2004design}.
The neuriteness measurement enhances bright, neurite-like structures whilst ignoring dark, line-like structures. Further, background intensity discontinuities that are immune to first order derivatives are suppressed by the use of second order derivatives.

\subsubsection{Regularized Volume Ratio}
\label{subsec:volrat}
Recently a new Hessian-based vessel enhancer was introduced in~\cite{JPLetal2016}. This approach attempts to resolve the drawbacks found in most previous Hessian-based approaches, which are directly proportional to $\lambda_2$ (2D and 3D) or $\lambda_3$ (3D only). These drawbacks are
\begin{enumerate*}
	\item the response of $\lambda_{2,3}$ is non-uniform within an ideal elongated or rounded structure of uniform intensity,
	\item the magnitude of $\lambda_{2,3}$ varies with image intensity, and
	\item enhancement is not uniform across scales.
\end{enumerate*}
Jerman et al.~\cite{JPLetal2016} attempt to solve this by modifying the volume ratio to indicate elongated structures, such as vessels, and regularising $\lambda_3$ (where $\lambda_3=\lambda_2$ in 2D) to ensure robustness to low magnitude changes, \eg~noise in regions of uniform intensity. Their final solution in 2D is given by
\begin{equation}
	\mathcal{V}_P=
	\begin{cases}
		0                                                                               & \text{if}~\lambda_2 \le 0 \lor \lambda_{\rho} \le 0, \\
		1                                                                               & \text{if}~\lambda_2 \ge \lambda_{\rho}/2 > 0,        \\
		\lambda_2^2(\lambda_{\rho} - \lambda_2)[\frac{3}{\lambda_2 + \lambda_{\rho}}]^3 & \text{otherwise},
	\end{cases}
\end{equation}
where $\lambda_{\rho}$ is the regularised form of $\lambda_2$, given by
\begin{equation}
	\lambda_{\rho}(s) =
	\begin{cases}
		\lambda_2                                  & \text{if}~\lambda_2 > \tau\max_\mathbf{p}\lambda_2(\mathbf{p},s),       \\
		\tau\max_\mathbf{p}\lambda_2(\mathbf{p},s) & \text{if}~0 < \lambda_2 \le \tau\max_\mathbf{p}\lambda_2(\mathbf{p},s), \\
		0                                          & \text{otherwise},
	\end{cases}
\end{equation}
for scale $s$, where $\tau$ is a cut-off threshold between zero and one and for any pixel $\mathbf{p}=(x,y)$.

\subsection{Phase Congruency-based Enhancement Methods}
\label{subsec:pc}
One problem with many image enhancement methods is that they depend, to some extent, on image intensity and therefore fine, and usually lower contrast, vessels may be missed. The development of a contrast-independent image enhancement measurement has been shown in~\cite{obara2012contrast}. This approach builds upon the idea of phase congruency: a measurement that looks for features at points where Fourier phase components align~\cite{morrone1986mach}.
The original measurement is sensitive to noise, one dimensional and not a robust detector of features; this has been extended by Kovesi and colleagues~\cite{kovesi2003phase,kovesi2000phase,kovesi1999image,kovesi1996invariant} into a 2D phase congruency approach that combines both sine and cosines phase elements, improving the localisation information and noise robustness.

\subsubsection{Phase Congruency Tensor}
\label{subsec:pct}
A contrast-independent, tensor-based measurement of local image behaviour, the phase congruency tensor, was introduced in~\cite{obara2012contrast} as a detector of curvilinear structures regardless of intensity variation and  inhomogeneous contrast issues. For a given set of scales ${s}$ and a given set of phase congruency measures $PC_s(\textbf{p},\theta)$ for each orientation $\theta$, the proposed phase congruency tensor is,
\begin{equation}
	\label{eq:phaseTensor}
	T_{PC_s} = \sum_{\theta}PC_s(\mathbf{p},\theta)
	(\mathbf{u}_\theta\mathbf{u}_\theta^T),
\end{equation}
where $\mathbf{u}_\theta$ is the normalised column vector for each orientation and $\alpha=1/{m-1}$, with $m$ is the image dimensionality.

The eigenvalues of the tensor can then be used in the same way as the Hessian matrix eigenvalues are used in \cref{eq:frangi,eq:neurite} to define PCT Neuriteness and Vesselness equivalents.
\vspace{-10px}

\subsection{Contrast Limited Adaptive Histogram Equalisation}
\label{subsec:clahe}
A somewhat simpler approach in image enhancement is to carry out contrast enhancement by histogram equalisation. A popular approach in this category is Contrast Limited Adaptive Histogram Equalisation (CLAHE)~\cite{Pisano1998}.
The image is first divided into small regions, each of which will undergo histogram equalisation (thus making the algorithm adaptive); however, this would also massively enhance noise and, to prevent this, contrast limiting is applied. Contrast limiting identifies any original histogram bin of the region with more than a set number pixels above a threshold and `clips' those pixels, distributing them evenly to the rest of the bins, histogram equalisation can then be carried out on the clipped region. Most implementations will also bilinearly interpolate two remove artefacts at the borders between regions.
\vspace{-15px}

\textcolor{red}{\subsection{Multiscale Wavelet Transform}
Another approach to enhance the vessels in retinal images is wavelet transform that proposed by~\cite{bankhead2012fast}, they have used wavelets and edge detection to develop a retinal vessel segmentation method. They used isotropic undecimated wavelet transform (IUWT) coefficients as a threshold to the segment of vessels. Then, its followed by cubic B-spline fitting to determine vessels profile. According to wavelet theory; scaling coefficients at each iteration can be obtained by using low-pass filters and wavelet coefficients by subtraction of scaling coefficients. 
\vspace{-10px}
\subsection{Mutliscale Line Detector}
Nguyen and his colleagues\cite{nguyen2013effective} proposed a multiscale line detector approach at various scales to localise vessels in retinal images. The basic line response identified by subtraction of average value and the maximum value of each pixel that is computed by 12 different line directions. The basic line detector has drawbacks; extend to crossover points, produce false vessel effect and tends to combine close vessels. To overcome this, \cite{nguyen2013effective} has pronounced multiscale line detector by generalising the basic line detector by varying length of the aligned lines.  }

\subsection{Enhancement with Mathematical Morphology}
\label{subsec:morph}
An alternative approach to image enhancement is the use of mathematical morphology. Morphological operations are a set of non-linear filtering methods formed through combination of two basic operators: dilation and erosion. Where we describe dilation, $\oplus$, for a point in a grayscale image, $I(\mathbf{p})$, as the maximum of the points in the weighted neighbourhood described by the structuring element $b(\mathbf{p})$ or, mathematically, as
\begin{equation}
	(I \oplus b)(\mathbf{p}) = \sup_{\mathbf{x} \in E}[I(\mathbf{x})+b(\mathbf{p}-\mathbf{x})],
\end{equation}
where $\sup$ is the supremum and $\mathbf{x} \in E$ denotes all points in Euclidean space within the image. Likewise, we describe the erosion, $\ominus$, as the minimum of the points in the neighbourhood described by the structuring element or, mathematically as
\begin{equation}
	(I \ominus b)(\mathbf{p}) = \inf_{\mathbf{x} \in E}[I(\mathbf{x})+b(\mathbf{p}-\mathbf{x})],
\end{equation}
where $\inf$ the infimum. Dilation is able to expand bright areas and reduce dark areas, whilst erosion expands dark areas reducing bright areas.

From these two operators we can define four commonly uses morphological filters:
\begin{align}
	\text{opening}:    & \quad (I \circ b)(\mathbf{p}) = ((I \ominus b) \oplus b)(\mathbf{p})   \\
	\text{closing}:    & \quad (I \bullet b)(\mathbf{p}) = ((I \oplus b) \ominus b)(\mathbf{p}) \\
	\text{top-hat}:    & \quad TH(I(\mathbf{p})) = I(\mathbf{p}) - (I \circ b)(\mathbf{p})      %\\
%	\text{bottom-hat}: & \quad BH(I(\mathbf{p})) = I(\mathbf{p}) - (I \bullet b)(\mathbf{p}).
\end{align}
Where an opening will preserve dark features and patterns, suppressing bright features, and a closing will preserve bright features whilst suppressing dark patterns. Top-hat transforms will enhance bright regions, where those patterns correspond to the structuring element used.

Mathematical morphology has been long used for image enhancement including noise suppression, feature detection and contrast sharpening. Traditionally, the top-hat transform is used to identify `peak' like signals. Modifications of the top hat approach have been used in, for example, pattern classification \cite{JWR2004}, enhancing depth-of-field \cite{DCC2006} and edge detection \cite{CWRTetal2002}.

\subsubsection{Top-Hats}
\label{subsubsec:top-hats}
%Mathematical morphology has been used by many researchers to enhance vessel-like structures~\cite{ZK2001,MC2006,MM2011,FBRetal2012,LJCetal2016,CCSetal2016}. 
One popular approach is proposed by Zana and Klein~\cite{ZK2001} where they combine morphological transforms and cross-curvature evaluation to segment vessel-like objects. This relies on the assumption that vessels are linear, connected and have smooth variations of curvature along the peak of the line. Their algorithm combines a sum of top hats, calculated using linear structuring elements at different angles and computation of the curvature, using a Laplacian of Gaussian approach, to reduce noise and enhance vessel-like signals.
\vspace{-10px}

\subsection{Limitations and Challenges}
\label{subsec:limits}
Due to the nature of many vessel enhancement approaches, \ie{} the supression of disk-like objects, many existing approaches loose intensity at junctions and some bends which are more locally similar to disk-like elements than extended vessel-like objects. Contrariwise, those methods that have been designed not to loose signal at these features often see vessel-like features everywhere in the image, including in noisy regions, and thus enhance `vessels' that are not there. Our approach, described in the next section neither supressed junctions and bends nor enhances noise as vessel-like structures.
\vspace{-10px}
