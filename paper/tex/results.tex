% !TEX root = ../main.tex

\section{Results}\label{sec:results}

% FIGURE
\input{./tex/figure-compare}

% FIGURE
\input{./tex/figure-psrn}

% FIGURE
\input{./tex/figure-illum}

In this section, the proposed method is qualitatively and quantitatively validated and compared with the existing state-of-the-art methods using synthetic and real image datasets, including retinal image datasets with human-annotated ground truths and other biomedical images.

As with any image processing method, an understanding of how the parameters involved affect the result is essential. In general, we have found the bowler-hat transform to be robust, usually requiring 10--12 $\theta$ orientations for line structuring element and the size of the disk/line structuring element $d$ to be greater than the thickest vessel structure in an image.

\subsubsection{Profile Analysis}

In \cref{fig:profile}, we plot the responses of the proposed and state-of-the-art enhancement methods on a synthetic, simple vessel-like structure. As can be seen, whilst the Hessian-based methods have an enhanced signal at the center of the vessel, \ie{} a peak value of one at the vessels centre-line, their value quickly drops off and decreases the perceived thickness of the vessel. Contrariwise, PCT-based methods do not necessarily peak at the vessel centre, but their response does not drop off quickly, and they maintain a higher response through to the edges of the vessel, \ie{} the perceived thickness is not decreased. The proposed method has both these benefits: a maximal peak value at the vessel centre-line and an enhanced response to the edges of the vessel. As a result the reliable vessel thicknesses can be captured.

\subsubsection{Response to Vessels, Intersections, and Blobs}\label{subsubsec:compare}

\cref{fig:compare} presents a qualitative comparison between the proposed method and the state-of-the-art methods when applied to synthetic images with vessel-like, intersection-like, and blob-like structures. Key issues that occur across the state-of-the-art methods include defects at junctions (purple arrows), noise enhancement, tip artefacts (orange arrows) and loss of signal (yellow arrows). These issues are all absent with our proposed method.

\subsubsection{Response to Noise}\label{subsubsec:noise}

\cref{fig:psnr} shows effect of three different noise types on the proposed and state-of-the-art methods. Given that the proposed method has no built-in noise suppression, it is unsurprising that the effect of noise on the enhanced image is in-line with the raw image. We note that the method is weakest in response to speckle noise (multiplicative Gaussian) and also weak in response to salt and pepper noise. This follows from the noise-sensitivity in morphological operations and should be taken into consideration when choosing an enhancement method.

\subsubsection{Response to Uneven Background Illumination}\label{subsubsec:illum}

\cref{fig:illum} presents the response of the proposed method to an uneven illumination scenario. Key features such as junctions are preserved and appear unaffected by even severe illumination problems. This ability to preserve junctions under uneven illumination is important for many real applications of vessel enhancement and the proposed method is able to do this, unlike the current state-of-the-art methods.

% FIGURE
\input{./tex/figure-roc}

% TABLE
\input{./tex/table-auc}

\subsection{Real Data - Retinal Image Datasets}\label{subsec:real}

In this section, we show the quality of the proposed method validated on three publicly available retinal image datasets: the DRIVE, STARE, and HRF databases. These datasets have been chosen because of their availability and their ground truth data. We have used these ground truth segmentations to quantitatively compare the proposed method with the other vessel enhancement methods.

The Digital Retinal Images for Vessel Extraction (DRIVE)~\cite{staal2004} dataset is a published database of retinal images for research and educational purposes. The database consists of twenty colour images that are JPEG compressed, as for many screening programs. These images were selected randomly from a screening of 400 diabetic subjects between the ages of 25 and 90. The ground truth provided with this dataset consists of a manual segmentation of the vasculature for each image. Ground truths were prepared by trained observers, and 'true' pixels are those for which observers where $> 70\%$ certain.

The STructured Analysis of the REtina (STARE) dataset is another publicly available database~\cite{hoover2000locating} containing twenty colour images with human-determined vasculature ground truth. We have compared all these images against the AH labelling.

The High-Resolution Fundus (HRF) image dataset~\cite{odstrcilik2013retinal} consists of 45 retinal images. This dataset has three type of subjects include  healthy, diabetic retinopathy, and glaucoma.

\subsubsection{Quantitative Validation}\label{subsec:quant}

Whilst a visual inspection can give some information regarding the effectiveness of the vessel enhancement method, a form of quantitative validation is required. In order to compare the proposed method with the other state-of-the-art methods, we have chosen to calculate the Receiver Operating Characteristic (ROC) curve and the Area Under the Curve (AUC). The sensitivity and specificity for the ROC are defined as:
\vskip5pt
\begin{align}
	\text{sensitivity} = \frac{TP}{TP+FN}, \\
	\text{specificity} = \frac{TN}{TN+FP},
\end{align}
where $TP$ is the true positive count, $FP$ the false positive count, $TN$ the true negative and $FN$ the false negative counts of the segmented pixels after thresholding the enhanced images with different threshold levels. These metrics take in an image, either the original or an enhanced image, and the ground truth image, for each data set. A higher AUC value indicates a better enhancement, with a value of $1$ indicating that the enhanced image is identical to the ground truth.

\subsubsection{Healthy Subjects}\label{subsubsec:real:compare}

 \cref{fig:hrf} shows the results of the proposed and state-of-the-art methods applied to a sample image from the HRF dataset (results for DRIVE and STARE datasets can be found in Supplementary Materials).
 
% FIGURE
\input{./tex/figure-hrf}
 
 We can see that the proposed method is able to enhance finer structures as detected by the human observer but not emphasised by many of the other methods (see arrows). 

% FIGURE
\input{./tex/figure-unhealthy}

We can also see that, whilst the connectivity seems to be maintained (unlike in \cref{fig:hrf:vessel}), 'false vessels' are not introduced (\cf{} \cref{fig:hrf:neurite}).

Finally, \cref{fig:roc} and \cref{tab:auc} present ROC curves and mean AUC values for the results of the proposed and state-of-the-art methods applied to a all images across the DRIVE, STARE and HRF datasets.

\subsubsection{Unhealthy Subjects}

\cref{fig:unhealthy} presents a visual comparison between the proposed method and the other state-of-the-art methods for sample images of subjects with diabetic retinopathy and with glaucoma from the DRIVE, STARE and HRF datasets.

\subsection{Other Biomedical Data}\label{subsubsec:other}
Although we have demonstrated above the vessel-like structures enhancement ability of the proposed method on retinal image datasets, the proposed method can also be used with a wide range of biomedical images, see \cref{fig:other}.
