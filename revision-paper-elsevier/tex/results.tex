% !TEX root = ../main-elsevier.tex
\section{Results}\label{sec:results}

In this section, the proposed method is qualitatively and quantitatively validated and compared with the existing state-of-the-art methods using synthetic and clinically relevant, retinal image datasets, with human-annotated ground truths, and other biomedical images.

As with any image processing method, an understanding of how the parameters involved affect the result is essential. In general, we have found the bowler-hat transform to be robust, usually requiring 10--12 $\theta$ orientations for line structuring element and the size of the disk/line structuring element $d$ to be greater than the thickest vessel structure in an image.

% FIGURE
\input{./tex/figure-profile}

\subsection{Profile Analysis}

\textcolor{red}{
The effect of the vessel enhancement methods on a cross-section profile of the enhanced vessels is demonstrate using a simple vessel-like structure in a synthetic image, see \Cref{fig:profile}. For a fair comparision, all enhanced images were normalised such that the brightest pixel in the whole image has a value of $1$ and the darkest a value of $0$. As we can notice, the enhancement methods tend to expand or shrink the vessel-like structures. } 
Moreover, whilst the Hessian-based methods have an enhanced signal at the center of the vessel, \ie{} a peak value of one at the vessels centre-line, their value quickly drops off and decreases the perceived thickness of the vessel. Contrariwise, PCT-based methods do not necessarily peak at the vessel centre, but their response does not drop off quickly, and they maintain a higher response through to the edges of the vessel, \ie{} the perceived thickness is not decreased. The proposed method has both these benefits: a maximal peak value at the vessel centre-line and an enhanced response to the edges of the vessel. As a result reliable vessel thickness can be captured. \vspace{-10pt}
% FIGURE
\input{./tex/figure-compare}
\subsection{Response to Vessels, Intersections, and Blobs}\label{subsubsec:compare}

\Cref{fig:compare} presents a qualitative comparison between the proposed method and the state-of-the-art methods when applied to synthetic images \textcolor{red}{and real images} with vessel-like, intersection-like, and blob-like structures. Key issues that occur across the state-of-the-art methods include defects at junctions (purple arrows), noise enhancement, tip artefacts (orange arrows) and loss of signal (yellow arrows). These issues are all absent with our proposed method. \vspace{-10pt}

% FIGURE
\input{./tex/figure-psrn}

\subsection{Response to Noise}\label{subsubsec:noise}

\textcolor{red}{
To test how the state-of-the-art enhancement methods and the proposed method behave with the different level and type of the noise, a noisy synthetic image that includes a single vessel-like structure was used. We generate a noisy image by optimising the noise generation parameters to achieve a target PSNR by using a genetic optimisation algorithm. We then examine the enhancement methods by increasing the noise level and then calculating the AUC values for each level and each method.} \Cref{fig:psnr} shows effect of three different noise types on the proposed and state-of-the-art methods. Given that the proposed method has no built-in noise suppression, it is unsurprising that the effect of noise on the enhanced image is in-line with the raw image. We note that the method is weakest in response to speckle noise (multiplicative Gaussian) and also weak in response to salt and pepper noise. This follows from the noise-sensitivity in morphological operations and should be taken into consideration when choosing an enhancement method.

% FIGURE
\input{./tex/figure-illum}

\subsection{Response to Uneven Background Illumination}\label{subsubsec:illum}

\Cref{fig:illum} presents the response of the proposed method to an uneven illumination scenario. Key features such as junctions are preserved and appear unaffected by even severe illumination problems. This ability to preserve junctions under uneven illumination is important for many real applications of vessel enhancement and the proposed method is able to do this, unlike the current state-of-the-art methods. 

\subsection{Real Data - Retinal Image Datasets}\label{subsec:real}

In this section, we show the quality of the proposed method validated on three publicly available retinal image datasets: the DRIVE, STARE, and HRF databases. These datasets have been chosen because of their availability and their ground truth data. We have used these ground truth segmentations to quantitatively compare the proposed method with the other vessel enhancement methods.

The Digital Retinal Images for Vessel Extraction (DRIVE)~\cite{staal2004} dataset is a published database of retinal images for research and educational purposes. The database consists of twenty colour images that are JPEG compressed, as for many screening programs. These images were selected randomly from a screening of 400 diabetic subjects between the ages of 25 and 90. The ground truth provided with this dataset consists of a manual segmentation of the vasculature for each image. Ground truths were prepared by trained observers, and 'true' pixels are those for which observers where $> 70\%$ certain.

The STructured Analysis of the REtina (STARE) dataset is another publicly available database~\cite{hoover2000locating} containing twenty colour images with human-determined vasculature ground truth. We have compared all these images against the AH labelling.

The High-Resolution Fundus (HRF) image dataset~\cite{odstrcilik2013retinal} consists of 45 retinal images. This dataset has three type of subjects include  healthy, diabetic retinopathy, and glaucoma.\vspace{-10pt}

\subsubsection{\textcolor{red}{Quantitative Validation - Enhancement}}\label{subsec:quant}

\textcolor{red}{While a visual inspection can give some information regarding the effectiveness of the vessel enhancement methods, a form of quantitative validation is required. Therefore, as proposed in~\cite{fawcett2006introduction}, we have used the Receiver Operating Characteristic (ROC) curve and the Area Under the Curve (AUC) metrics to compare the vessel enhancement methods. To derive the ROC curve and then to calculate the AUC value, each enhanced image is segmented at different thresholds and compared with the corresponding ground truth sgmentation. Such procedure is used for~\Cref{fig:psnr},~\Cref{fig:roc}, and ~\Cref{tab:auc}.}

\subsubsection{\textcolor{red}{Quantitative Validation - Segmentation}}\label{subsec:quant:seg}

\textcolor{red}{To quantitatively evaluate the robustness of the vessel segmentation methods, sensitivity (SE), specificity (SP), and accuracy (ACC) metrics are calculates for each segmented image and its corresponding ground truth sgmentation, as follows:} 

\textcolor{red}{
\begin{align}
	&\text{SE} = \frac{TP}{TP+FN},\\
	&\text{SP} = \frac{TN}{TN+FP}, \\
	&\text{ACC} = \frac{TP+TN}{TP+TN+FP+FN},
\end{align}}

\textcolor{red}{where $TP$ is the true positive count, $FP$ the false positive count, $TN$ the true negative and $FN$ the false negative counts of the segmented pixels. 
We used these metrics in~\Cref{tab:acc} and~\Cref{tab:seg}.}


\subsubsection{Healthy Subjects}\label{subsubsec:real:compare}

\Cref{fig:hrf} shows the results of the proposed and state-of-the-art methods applied to a sample image from the HRF dataset (results for DRIVE and STARE datasets can be found in Supplementary Materials).

We can see that the proposed method is able to enhance finer structures as detected by the human observer but not emphasised by many of the other methods (see arrows). 

We can also see that, whilst the connectivity seems to be maintained (unlike in \Cref{fig:hrf:vessel}), `false vessels' are not introduced (\cf{} \Cref{fig:hrf:neurite}).

Finally, \Cref{fig:roc} and \Cref{tab:auc} present ROC curves and mean AUC values for the \textcolor{red}{enhancement} results of the proposed and state-of-the-art methods applied to all images across the DRIVE, STARE and HRF datasets \textcolor{red}{by using the quantitative validation as described~\Cref{subsec:quant}.}  

% FIGURE
\input{./tex/figure-roc}
% TABLE
\input{./tex/table-auc}

\subsubsection{Unhealthy Subjects}

\textcolor{red}{\Cref{fig:unhealthy} presents a visual comparison of the enhancement methods applied to sample images of subjects with diabetic retinopathy and with glaucoma from the DRIVE, STARE and HRF datasets. As we can notice in~\Cref{fig:pathology:stareResult}, the proposed method is quite sensitive to noisy regions. This issue can be addressed by a use of a line-shaped morphological structuring element with a varying thickness. 
Even so, the proposed method achieved a high score overall on the HRF unhealthy images as illustrated in~\Cref{tab:auc}.}\vspace{-10pt}

\subsubsection{\textcolor{red}{Enhancement with Local Thresholding}}\label{subsubsec:seg}

\textcolor{red}{\Cref{fig:seg} and ~\Cref{tab:acc} demonstrate the vessel segmentation results obtained by the proposed and the state-of-the-art vessel-like structures enhancement methods followed by the same local thresholding approach proposed in~\cite{jiang2003adaptive} when applied to the HRF dataset images.}

%FIGURE
\input{./tex/figure-hrf}
%FIGURE
\input{./tex/figure-unhealthy}
% FIGURE
\input{./tex/figure-segmentation}
%TABLE
\input{./tex/table-acc}
%TABLE
\input{./tex/table-seg}

\subsubsection{\textcolor{red}{Comparison with Other Segmentation Methods}}\label{subsubsec:seg-comparion}

\textcolor{red}{To highlight the effectiveness of the proposed vessel enhancement method (combined with the local thresholding approach~\cite{jiang2003adaptive}) for a full vessel segmentation, we compared the performance of our method with seventeen state-of-the-art vessel segmentation methods reported in the literature \cite{staal2004,soares2006retinal,lupascu2010fabc, you2011segmentation,marin2011new,wang2013retinal,mendonca2006segmentation,palomera2010parallel,martinez2007segmentation,al2009active,FBRetal2012,nguyen2013effective,bankhead2012fast,orlando2014learning,azzopardi2015trainable,zhang2016robust,odstrcilik2013retinal} applied to DRIVE, STARE and HRF datasets.}
\textcolor{red}{
\Cref{tab:seg} illustrates the reported results of the seventeen segmentation methods compared with the proposed method. As can be seen from \Cref{tab:seg}, the proposed method is at least comparable, in performance, even where it does not outperform the other methods.}
\textcolor{red}{The results on both datasets demonstrate that the sensitivity of the proposed method is not in top three respectively for DRIVE ($SE=0.616$) and STARE ($SE=0.730$). However, the proposed method has the highest score with the specificity ($SP=0.991$) for DRIVE and ($SP=0.979$) for STARE. Most importantly, our method has the accuracy ($ACC=0.960$) and ($ACC=0.962$) for DRIVE and STARE respectively; the highest compared to other vessel segmentation methods. Finally, the proposed method has the highest score for HRF dataset, with ($SE=0.831$), ($SP=0.981$) and ($ACC=0.963$). }

\subsection{Other Biomedical Data}\label{subsubsec:other}
While we have demonstrated the proposed method on the enhancement of vessel-like structures, the approach is feasible for a wide range of biomedical images, see \Cref{fig:other}.
% FIGURE
\input{./tex/figure-others}