% !TEX root = ../main-elsevier.tex

\section{Method}\label{sec:methods}

In this section, we introduce our novel, mathematical morphology-based method for vessel-like structure enhancement in images: the bowler-hat transform. We highlight the key concepts that allow this method to address the major drawbacks of existing, state-of-the-art methods.

\subsection{Mathematical Morphology}

Morphological operations are a set of non-linear filtering methods formed through a combination of two basic operators: dilation and erosion.

Dilation, ($\oplus$), for a given pixel in greyscale image, $I(\mathbf{p})$, can be described as the maximum of the points in the weighted neighbourhood described by the structuring element $b(\mathbf{p})$, and mathematically:
\begin{equation}
	(I \oplus b)(\mathbf{p}) = \sup_{\mathbf{x} \in E}[I(\mathbf{x})+b(\mathbf{p}-\mathbf{x})],
\end{equation}
where `$\sup$' is the supremum and $\mathbf{x} \in E$ denotes all points in Euclidean space within the image. Likewise, we mathematically describe the erosion ($\ominus$), as the minimum of the points in the neighbourhood described by the structuring element:
\begin{equation}
	(I \ominus b)(\mathbf{p}) = \inf_{\mathbf{x} \in E}[I(\mathbf{x})+b(\mathbf{p}-\mathbf{x})],
\end{equation}
where `$\inf$' is the infimum. Dilation is able to expand bright areas and reduce dark areas, whilst erosion expands dark areas reducing bright areas as detailed in~\cite{haralick1987image}.

From these two operators we can define two commonly used morphological filters:
\begin{align}
	\text{opening}:    & \quad (I \circ b)(\mathbf{p}) = ((I \ominus b) \oplus b)(\mathbf{p})   \\
	\text{closing}:    & \quad (I \bullet b)(\mathbf{p}) = ((I \oplus b) \ominus b)(\mathbf{p})
\end{align}
where an opening ($\circ$) will preserve dark features and patterns, suppressing bright features, and a closing ($\bullet$) will preserve bright features whilst suppressing dark patterns.

\subsection{Proposed Method}\label{subsec:proposed}

\Cref{fig:flow} presents a flow diagram of the proposed method which combines the outputs of morphological operations upon an image carried out with two different banks of structural elements: one bank of disk elements with varying radii, and one bank of line elements with varying radii and rotation.

For a given greyscale input image, $I$, we carry out a series of morphological openings with a bank of disk-shaped structuring elements, $b_d$ of diameter $d\in{[1,d_{max}]}$ pixels, where $d_{max}$ is the expected maximum vessel size and user defined parameter. This produces a stack of images, for all $d$, such that
	\begin{equation}
		\{I_{disk}\} = \{I \circ b_d \colon \forall d \in [1,d_{max}]\}.
	\end{equation}
	In each $I_{disk}$ image, vessel segments wider than $d$ remain and those segments smaller than $d$ are removed.

We also produce a similar stack of images using a bank of line-shaped structuring elements, $b_{d,\theta}$; each line-shaped is of length $d\in{[1,d_{max}}]$, with a width of $1$ pixel, and orientation $\theta\in{[0,180)}$, $\theta_{sep}$ is angle step.

As a result, vessel segments that are longer than $d$ and along the direction defined by $\theta$ will remain, and those shorter than $d$ or along the direction defined by $\theta$ will be removed. For each line length $d$ we produce a stack of images for all orientations defined by $\theta\in{[0,180)}$. Then, for each $d$, we calculate a single image, $I_{line}$ as a pixel-wise maximum of the stack such that
	\begin{equation}
		\{I_{line}\} = \{\max_\theta(\{I \circ b_{d,\theta} \colon \forall \theta\}) \colon \forall d \in [1,d_{max}]\}.
	\end{equation}

These two stacks, $\{I_{disk}\}$ and $\{I_{line}\}$, are then combined by taking the stack-wise difference, the difference between the maximum opening with a line of length $d$ across all angles and an image formed of opening with a disk of size $d$, to form the enhanced image. The final enhanced image is then formed from maximum difference at each pixel across all stacks,
	\begin{equation}
		I_{enhanced} = \max_d(\lvert I_{line} - I_{disk}\rvert).
	\end{equation}

Pixels in the background, \ie{} dark regions, will have a low value due to the use of openings; pixels in the foreground of blob-like structures will have a low value as the differences will be minimal, \ie{} similar values for disk-based and line-based openings; and pixels in the foreground of vessel-like structures will have a high value, \ie{} large differences between longer line-based openings and disk-based openings.

The combination of line and disk elements gives the proposed method a key advantage over the existing methods. Given an appropriate $d_{max}$, \ie{} larger than any vessels in the image, a junction should appear bright like those vessels joining that junction, something that many other vessel enhancement methods fail to do. This is due to the ability to fit longer line-based structural elements within the junction area. As a result, the vessels network stays connected when enhanced and segmented, especially at junctions.

In \Cref{sec:results}, we demonstrate, qualitatively and quantitatively, the key advantages of the bowler-hat transform over the existing, state-of-the-art vessel-like structure enhancement methods.

\subsection{Implementation}\label{subsec:imp}

All codes were implemented and written in MATLAB 2016b \cite{matlab} on Windows 8.1 Pro 64-bit PC running an Intel Core i7-4790 CPU (3.60 GHz) with 16GB RAM. The source code is available in a GitHub repository (\textbf{TBC}).
