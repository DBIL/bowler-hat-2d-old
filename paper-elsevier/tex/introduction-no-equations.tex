% !TEX root = ../main-elsevier.tex
\section{Introduction}\label{sec:intro}

Many biomedical images contain vessel-like structures, such as blood vessels or cytoskeletal networks~\cite{Bibiloni2016}. Automated extraction of these structures and their connected network is often an essential step in quantitative image analysis and computer-aided diagnostic pipelines. For example, automated retinal vessel extraction is used for diagnosis, screening, and evaluation in a wide range of retinal diseases, including diabetes and arteriosclerosis~\cite{Fraz2012}.

However, for a multitude of reasons, \eg{} noisy image capture, sample/patient variability, low contrast scenarios, etc., biomedical imaging modalities may suffer from poor quality.
As such standard image segmentation methods are not able to robustly detect vessel-like structures, and therefore some form of vessel-like structure enhancement is required~\cite{Bibiloni2016}.

A wide range of vessel enhancement methods have been proposed (see~\cite{Fraz2012} and~\cite{Bibiloni2016} for a recent review). These include Hessian~\cite{frangi1998multiscale,Su2012,Su2014}, Phase Congruency Tensor~\cite{obara2012contrast,obara2012coherence}, mathematical morphology~\cite{ZK2001,Su2014,lu2016vessel}, adaptive histogram equalisation~\cite{Pisano1998} based approaches and many others~\cite{Feng2007,FBRetal2012,bankhead2012fast,Azzopardi2013,nguyen2013effective,Sigurosson2014,CCSetal2016,Merveille2017}.

However, many of these methods still have considerable issues when faced with variations in contrast, high levels of noise, variation in image features (\eg{} lines vs junctions; retention of network connectivity), and complexity of method parameter space.

\subsection{Contribution and Organisation}

In this paper, we introduce a new enhancement method for vessel-like structures based on mathematical morphology, which exploits a key shape property of vessel-like structures: elongation. The proposed method, called the bowler-hat transform, has been qualitatively and quantitatively validated and compared with state-of-the-art methods using a range of synthetic data and available retinal image datasets. 
The obtained results show that the proposed method achieves high-quality, vessel-like structure enhancement in both synthetic examples and clinically relevant retinal images. The method is suitable for a range of biomedical image types without needing prior training or tuning. Finally, we have made the implementation of our approach available online, along with source code and all test functions.

The rest of this paper is organised as follows. In \Cref{sec:related}, we introduce existing vessel-like structure enhancement methods and highlight their known limitations. \Cref{sec:methods} introduces and explains the proposed bowler-hat transform, \Cref{sec:results} presents validation experiments and results on synthetic and real data. Finally, in \Cref{sec:Conclusion}, we discuss the results and future work.

% -------------------------------------------------------------------------
\section{Related Work}\label{sec:related}

\subsection{Hessian-based Methods}\label{subsec:hessian}

In \cite{frangi1998multiscale}, Frangi et al. introduce a novel Hessian-based multi-scale concept for 2D curvilinear/3D tubular structure enhancement in images. They construct the Hessian matrix using second-order Gaussian derivatives. The eigenvectors and eigenvalues of the Hessian matrix then define the principal directions of local image features. These can then be combined to form different measures of vesselness or blobness~\cite{Bibiloni2016} in biomedical images.

\subsubsection{Vesselness}\label{subsec:ves}

The vesselness measure is proportional to the ratio of the eigenvalues \cite{frangi1998multiscale}. If the magnitude of both eigenvalues is small, \ie{} the local image structure is likely to be background, then the vesselness measure is small. If one eigenvalue is small and the other large then the local structure is likely to be vessel-like and the vesselness measure is large. Finally, if both eigenvalues are high then the structure is likely to be blob-like and the vesselness measure is again small.

This approach, however, leads to a failure at the intersection of vessels as both eigenvalues have similarly large values leading to a vesselness measure close to zero. Thus, vessel-like structures can be lost at junctions and therefore vessels network connectivity may be lost \cite{krissian2003multiscale}. An extension of this approach can be found in \cite{Su2014} where a multi-scale morpho-Gaussian filter is combined with multi-scale Hessian measurement to enhance the curvilinear features and reduce noise. 

\subsubsection{Neuriteness}\label{subsec:neurite}

As an alternative to vesselness, Meijering and colleagues~\cite{meijering2004design} introduce the neuriteness measure to enhance low contrast and highly inhomogeneous neurites in bioimages. Using a modified Hessian, with a tuning parameter, and a different combination of eigenvalues, neuriteness infers a putative neurite in every pixel of the image that has a non-zero value. Background intensity discontinuities that are immune to first order derivatives are suppressed by the use of second order derivatives.

A major failing for the neuriteness measure is that background noise signals are enhanced as if they are curvilinear structures. In the original paper~\cite{meijering2004design} this is solved with a tracing stage; however, as an enhancer only, this can cause serious problems for further analysis. The neuriteness measure also leads to a failure at the intersection of vessels as both eigenvalues have similarly large values leading to a neuriteness measure close to zero.
A further example of their work is found in~\cite{smafield2015automatic}.

\subsubsection{Regularized Volume Ratio}\label{subsec:volrat}

Recently, Jerman and colleagues~\cite{JPLetal2016} propose a new Hessian-based vessel enhancement method, which is able to resolve the drawbacks found in most of the previous Hessian-based methods:
\begin{enumerate*}
\item eigenvalues are non-uniform throughout an elongated or rounded structure that has uniform intensity;
\item eigenvalues vary with image intensity; and
\item enhancement is not uniform across scales.
\end{enumerate*}
To address such drawbacks, a modified volume ratio is introduced to ensure method robustness to low magnitude intensity changes in the image. A major issue of this method is the false vessel affect, as shown in \Cref{fig:hrf:vr} sensitivity. 

\subsection{Phase Congruency Tensor-based Methods}\label{subsec:pc}

A major issue with many image enhancement methods is that they depend, to some extent, on image intensity and, therefore, fine, and usually lower intensity, vessels may be missed. To address this issue a contrast-independent method, based on Phase Congruency (PC), was introduced in~\cite{kovesi2003phase}. This approach builds upon the idea of phase congruency, which looks for image features in the Fourier domain. 

The development of a contrast-independent, image enhancement measurement built upon PC has been shown in~\cite{obara2012contrast}. The Phase Congruency Tensor (PCT) is built upon PC principles but the tensor is decomposed. The calculated eigenvalues are then used in the same way as Hessian eigenvalues (see \Cref{subsec:ves,subsec:neurite}), to define PCT vesselness and PCT neuriteness measures. An extension of this method into 3D has recently been shown in~\cite{Sazak2017}.

A major drawback of the PC-based concept is the complexity of its parameter space. Moreover, as with Hessian-based measures, the PCT-based measures also lead to a failure at the intersection of vessels as both eigenvalues have similar, large values leading to PCT-based vesselness and neuriteness measures close to zero. \vspace{-10pt}

\subsection{Adaptive Histogram Equalisation-based Methods}\label{subsec:clahe}

Contrast Limited Adaptive Histogram Equalisation (CLAHE)~\cite{Pisano1998}, originally developed for spiculations enhancement in mammograms, is widely used for vessel enhancement. In this simple, histogram-based method an image is first divided into small regions, each of which then undergoes a histogram equalisation. To avoid over-enhancement of noise, a contrast limiting procedure is applied between regions. Further development of this method is demonstrated in~\cite{Zhao2014} where CLAHE is combined with an anisotropic diffusion filter to smooth the image and preserve vessel boundaries. A major drawback of this method is the noise sensitivity. 

\subsection{Wavelet Transform-based Enhancement Methods}\label{subsuce:wavelet}

Bankhead and colleagues~\cite{bankhead2012fast} propose the use of wavelets for vessel enhancement and segmentation. They calculate an isotropic, undecimated wavelet transform using the cubic B-spline mother wavelet, and employ the coefficients to the threshold steps for enhancement, followed by vessel segmentation. Further improvement of this approach is demonstrated in~\cite{Zhang2017} where multi-orientation and multi-scale features from the vessel filtering and the wavelet transform stages are combined and then used for training the random forest classifier.
A major drawback of this method is the complexity of its parameter space. 

\subsection{Line Detector-based Enhancement Methods}\label{subsec:linedetector}

Vessel-like feature enhancement has also been done using multi-scale line detectors~\cite{nguyen2013effective}. The basic line response, identified by subtraction of average value and the maximum value of each pixel, is computed at 12 different line directions. A major drawback of this method is at crossover points, where the method produces `false vessels' by merging nearby vessels. 
Further improvement of this method is demonstrated in~\cite{hou2014automatic} where a linear combination of all the line responses at varying scales is proposed to produce the final enhancement and segmentation.

\subsection{Mathematical Morphology-based Enhancement Methods}\label{subsec:morph}

Zana and Klein~\cite{ZK2001} proposed a novel method which combines morphological transforms and cross-curvature evaluation for vessel-like structure enhancement and segmentation. This method relies on the assumption that vessels are linear, connected and have smooth variations of curvature along the peak of the feature. First, a sum of top hats is calculated using linear structuring elements at different angles, then a curvature measure is calculated using a Laplacian of Gaussian, and finally, both of them are combined to reduce noise and enhance vessel-like structures in an image. 
Further improvement of this method is demonstrated in~\cite{Su2014,Sigurosson2014,lu2016vessel}. In particular, in~\cite{Sigurosson2014}, an advanced morphological directional filter called path openings is linked with data fusion based on fuzzy set theory. And most recently, in~\cite{lu2016vessel}, a multi-scale morphological top hats transform is combined with Gabor and matched filter.
A major issue with this method is that is quite slow and sensitive to noise. 
% FIGURE
\input{./tex/figure-flow}

\subsection{Limitations and Challenges}\label{subsec:limits}

Many existing vessel-like structure enhancement methods still have substantial issues when faced with variations in contrast (low-accuracy enhancement), high levels of noise (introduction of 'false vessels' effect), dealing with junctions/bends (suppression of disk-like structures; vessels network connectivity is lost), large image size (high computing time), and complexity of parameter space.
